import pbr.version

__version__ = pbr.version.VersionInfo(
    'python-landbclient').version_string()
