"""Landb unit test suite."""

import logging
import urllib3

from unittest import mock
from unittest import TestCase

from landbclient.main import main


@mock.patch('landbclient.main.LanDB')
class TestMain(TestCase):
    """Landb Unit tests."""

    def setUp(self):
        logging.getLogger('suds').setLevel(logging.INFO)
        urllib3.disable_warnings()
        logging.disable(logging.CRITICAL)

    def test_empty_params(self, mock_landb):
        """Test empty parameters."""
        main([])

    def test_complete_command(self, mock_landb):
        """Test complete command."""
        main(['complete'])

    def test_help_command(self, mock_landb):
        """Test help command."""
        main(['help'])

    def test_soap(self, mock_landb):
        """Test cluster-dev."""
        landb = mock_landb.return_value

        main(['soap', 'fake', 'param1', 'param2'])

        getattr(landb.soap, 'fake').assert_called_with(
            'param1',
            'param2'
        )

    def test_cluster_dev(self, mock_landb):
        """Test cluster-dev."""
        landb = mock_landb.return_value

        main(['cluster-dev', 'cluster'])

        landb.cluster_devices.assert_called_with(
            'cluster'
        )

    def test_cluster_info(self, mock_landb):
        """Test cluster-info."""
        landb = mock_landb.return_value
        landb.cluster_info.return_value = None

        main(['cluster-info', 'cluster', 'device'])

        landb.cluster_info.assert_called_with(
            cluster='cluster',
            device='device'
        )

    def test_device_managed_true(self, mock_landb):
        """Test dev-managed."""
        landb = mock_landb.return_value

        main(['dev-managed', 'device'])

        landb.device_managed.assert_called_with(
            'device',
            'true'
        )

    def test_device_managed_false(self, mock_landb):
        """Test dev-managed."""
        landb = mock_landb.return_value

        main(['dev-managed', 'device', '--false', 'false'])

        landb.device_managed.assert_called_with(
            'device',
            'false'
        )

    def test_device_random(self, mock_landb):
        """Test dev-random."""
        landb = mock_landb.return_value

        main(['dev-random'])

        landb.device_random.assert_called_with()

    def test_device_exists(self, mock_landb):
        """Test dev-exists."""
        landb = mock_landb.return_value

        main(['dev-exists', 'device'])

        landb.device_exists.assert_called_with(
            'device'
        )

    def test_device_exists_fail(self, mock_landb):
        """Test dev-exists."""
        landb = mock_landb.return_value
        landb.device_exists.return_value = None

        main(['dev-exists', 'device'])

        landb.device_exists.assert_called_with(
            'device'
        )

    def test_device_search(self, mock_landb):
        """Test dev-search."""
        landb = mock_landb.return_value

        main([
            'dev-search',
            '--name', 'device',
            '--ip', '127.0.0.1',
            '--mac', '00:00:00:00:00:00',
            '--surname', 'fake',
            '--serialnumber', 'fakeserial',
            '--tag', 'mytag',
        ])

        landb.device_search.assert_called_with(
            name='device',
            ip_addr='127.0.0.1',
            mac='00:00:00:00:00:00',
            surname='fake',
            serialnumber='fakeserial',
            tag='mytag'
        )

    def test_device_search_fail(self, mock_landb):
        """Test dev-search."""
        landb = mock_landb.return_value
        landb.device_search.return_value = None

        main([
            'dev-search',
            '--name', 'device',
            '--ip', '127.0.0.1',
            '--mac', '00:00:00:00:00:00',
            '--surname', 'fake',
            '--serialnumber', 'fakeserial',
            '--tag', 'mytag',
        ])

        landb.device_search.assert_called_with(
            name='device',
            ip_addr='127.0.0.1',
            mac='00:00:00:00:00:00',
            surname='fake',
            serialnumber='fakeserial',
            tag='mytag'
        )

    def test_device_info_(self, mock_landb):
        """Test dev-info."""
        landb = mock_landb.return_value
        landb.device_info.return_value = 'device'
        main(['dev-info', 'device'])

        landb.device_info.assert_called_with(
            'device'
        )

    def test_device_info_fails(self, mock_landb):
        """Test dev-info."""
        landb = mock_landb.return_value
        landb.device_info.return_value = None
        main(['dev-info', 'device'])

        landb.device_info.assert_called_with(
            'device'
        )

    def test_device_ipv6ready(self, mock_landb):
        """Test dev-setip6."""
        landb = mock_landb.return_value
        main(['dev-setip6', 'device', 'true'])

        landb.device_ipv6ready.assert_called_with(
            'device',
            True
        )

    def test_device_ipv6ready_fail(self, mock_landb):
        """Test dev-setip6."""
        landb = mock_landb.return_value
        landb.device_ipv6ready.return_value = None

        main(['dev-setip6', 'device', 'true'])

        landb.device_ipv6ready.assert_called_with(
            'device',
            True
        )

    def test_vm_create(self, mock_landb):
        """Test vm-create."""
        landb = mock_landb.return_value

        main([
            'vm-create',
            'vm',
            'subnet',
            '127.0.0.1',
            'fe80::1',
            '00:00:00:00:00:00',
            'localhost',
            'vmcluster',
            'r_name',
            'r_surname',
            '--username', 'u_name',
            '--usersurname', 'u_surname',
            '--managername', 'm_name',
            '--managersurname', 'm_surname',
        ])

        landb.vm_create.assert_called_with(
            vm_name='vm',
            vm_ipservice='subnet',
            vm_ip='127.0.0.1',
            vm_ipv6='fe80::1',
            vm_mac='00:00:00:00:00:00',
            vm_parent='localhost',
            vm_cluster='vmcluster',
            vm_responsible={
                'FirstName': 'r_name',
                'Name': 'r_surname',
            },
            vm_location={
                'Floor': 0,
                'Room': 0,
                'Building': 0,
            },
            vm_manufacturer='KVM',
            vm_model='VIRTUAL MACHINE',
            vm_os={
                'Name': 'LINUX',
                'Version': 'UNKNOWN',
            },
            vm_user={
                'FirstName': 'u_name',
                'Name': 'u_surname',
            },
            vm_landb_manager={
                'FirstName': 'm_name',
                'Name': 'm_surname',
            },
            vm_tag='OPENSTACK VM',
            manager_locked=True
        )

    def test_vm_add_interface(self, mock_landb):
        """Test vm-add-interface."""
        landb = mock_landb.return_value

        main([
            'vm-add-interface',
            'vm',
            'interface',
            'subnet',
            '127.0.0.1',
            'fe80::1',
            '00:00:00:00:00:00',
            'vmcluster',
        ])

        landb.vm_add_interface.assert_called_with(
            vm_device='vm',
            vm_interface_name='interface',
            vm_ipservice='subnet',
            vm_ip='127.0.0.1',
            vm_ipv6='fe80::1',
            vm_mac='00:00:00:00:00:00',
            vm_cluster='vmcluster',
            interface_only=False,
        )

    def test_vm_add_interface_only(self, mock_landb):
        """Test vm-add-interface."""
        landb = mock_landb.return_value

        main([
            'vm-add-interface',
            'vm',
            'interface',
            'subnet',
            '127.0.0.1',
            'fe80::1',
            '00:00:00:00:00:00',
            'vmcluster',
            '--interface-only', 'True',
        ])

        landb.vm_add_interface.assert_called_with(
            vm_device='vm',
            vm_interface_name='interface',
            vm_ipservice='subnet',
            vm_ip='127.0.0.1',
            vm_ipv6='fe80::1',
            vm_mac='00:00:00:00:00:00',
            vm_cluster='vmcluster',
            interface_only=True,
        )

    def test_vm_remove_interface(self, mock_landb):
        """Test vm-remove-interface."""
        landb = mock_landb.return_value

        main([
            'vm-remove-interface',
            'vm',
            'interface',
            '00:00:00:00:00:00',
        ])

        landb.vm_remove_interface.assert_called_with(
            vm_device='vm',
            vm_interface_name='interface',
            vm_mac='00:00:00:00:00:00',
        )

    def test_bind_interface(self, mock_landb):
        """Test bind-interface."""
        landb = mock_landb.return_value

        main(['bind-interface', 'interface_name', 'mac_address'])

        landb.cluster_devices.bind_unbind_interface(
            vm_interface_name='interface_name',
            mac_address='mac_address'
        )

    def test_vm_add_card(self, mock_landb):
        """Test vm-add-card."""
        landb = mock_landb.return_value

        main([
            'vm-add-card',
            'vm',
            '00:00:00:00:00:00',
        ])

        landb.vm_add_card.assert_called_with(
            vm_device='vm',
            mac_address='00:00:00:00:00:00',
        )

    def test_vm_remove_card(self, mock_landb):
        """Test vm-remove-card."""
        landb = mock_landb.return_value

        main([
            'vm-remove-card',
            'vm',
            '00:00:00:00:00:00',
        ])

        landb.vm_remove_card.assert_called_with(
            vm_device='vm',
            mac_address='00:00:00:00:00:00',
        )

    def test_vm_delete_destroy(self, mock_landb):
        """Test vm-delete."""
        landb = mock_landb.return_value

        main([
            'vm-delete',
            'vm',
        ])

        landb.vm_delete.assert_called_with(
            'vm',
            destroy=True,
        )

    def test_vm_delete_rename(self, mock_landb):
        """Test vm-delete."""
        landb = mock_landb.return_value

        main([
            'vm-delete',
            'vm',
            '--destroy', False,
        ])

        landb.vm_delete.assert_called_with(
            'vm',
            destroy=False,
        )

    def test_vm_delete_multiple(self, mock_landb):
        """Test vm-delete."""
        landb = mock_landb.return_value

        main([
            'vm-delete',
            'vm1', 'vm2',
        ])

        landb.vm_delete.assert_has_calls([
            mock.call(
                'vm1',
                destroy=True,
            ),
            mock.call(
                'vm2',
                destroy=True,
            ),
        ])

    def test_vm_migrate(self, mock_landb):
        """Test vm-migrate."""
        landb = mock_landb.return_value

        main([
            'vm-migrate',
            'vm',
            'localhost',
        ])

        landb.vm_migrate.assert_called_with(
            'vm', 'localhost'
        )

    def test_ipsrv_register(self, mock_landb):
        """Test ipsrv_register."""
        landb = mock_landb.return_value

        main([
            'ipsrv-register',
            'vm',
            'localhost',
        ])

        landb.ipservice_register.assert_called_with(
            'vm', 'localhost'
        )

    def test_service_info(self, mock_landb):
        """Test service_info."""
        landb = mock_landb.return_value

        main([
            'service-info',
            '--cluster', 'vmcluster',
            '--device', 'localhost',
        ])

        landb.service_info.assert_called_with(
            cluster='vmcluster',
            device='localhost'
        )
