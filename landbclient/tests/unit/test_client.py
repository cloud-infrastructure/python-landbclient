"""Landb unit test suite."""

import logging
import urllib3

from unittest import mock
from unittest import TestCase

from landbclient.client import LanDB
from landbclient.tests.unit import fixtures


@mock.patch('suds.client.Client')
class TestClient(TestCase):
    """Landb Unit tests."""

    MANAGER_USER = {
        'FirstName': 'E-GROUP',
        'Name': 'AI-OPENSTACK-ADMIN',
        'Department': 'IT'
    }

    def setUp(self):
        logging.getLogger('suds').setLevel(logging.INFO)
        urllib3.disable_warnings()

    def test_device_random(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.side_effect = iter([
            'exists1',
            'exists2',
            None
        ])
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_random()

    def test_device_random_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.return_value = 'exists'
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.device_random()

    def test_device_exists(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.return_value = 'fake'
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        self.assertTrue(landb.device_exists('test'))
        client.service.getDeviceInfo.assert_called_with(
            'test'
        )

    def test_device_exists_fail(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.return_value = None
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        self.assertFalse(landb.device_exists('test'))
        client.service.getDeviceInfo.assert_called_with(
            'test'
        )

    def test_device_exists_exception(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.side_effect = Exception('error')
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        self.assertFalse(landb.device_exists('test'))
        client.service.getDeviceInfo.assert_called_with(
            'test'
        )

    def test_device_info(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.return_value = 'fake'
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        self.assertIsNotNone(landb.device_info('test'))
        client.service.getDeviceInfo.assert_called_with(
            'test'
        )

    def test_device_info_fail(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.return_value = None
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.device_info('test')
        client.service.getDeviceInfo.assert_called_with(
            'test'
        )

    def test_device_managed(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.vmSetUnsetManagedFlag.return_value = 'fake'
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_managed('vm')
        client.service.vmSetUnsetManagedFlag.assert_called_with(
            'vm',
            'true'
        )

    def test_device_managed_fail(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.vmSetUnsetManagedFlag.return_value = None
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.device_managed('vm', 'false')
        client.service.vmSetUnsetManagedFlag.assert_called_with(
            'vm',
            'false'
        )

    def test_device_hostname(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.device_search.return_value = (
            fixtures.DEVICE
        )

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_hostname('127.0.0.1')
        client.service.searchDevice.assert_called_with(
            {
                'Name': '',
                'IPAddress': '127.0.0.1',
                'HardwareAddress': '',
                'Surname': '',
                'SerialNumber': '',
                'Tag': '',
            }
        )

    def test_device_search(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.device_search.return_value = (
            fixtures.DEVICE
        )

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_search(
            name='vm',
            ip_addr='127.0.0.1',
            mac='00:00:00:00:00:00',
            surname='fake',
            serialnumber='fakeserial',
            tag='mytag'
        )
        client.service.searchDevice.assert_called_with(
            {
                'Name': 'vm',
                'IPAddress': '127.0.0.1',
                'HardwareAddress': '00:00:00:00:00:00',
                'Surname': 'fake',
                'SerialNumber': 'fakeserial',
                'Tag': 'mytag',
            }
        )

    def test_device_search_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_search(
            name='vm',
            ip_addr='127.0.0.1',
            mac='00:00:00:00:00:00',
            surname='fake',
            serialnumber='fakeserial',
            tag='mytag'
        )
        client.service.searchDevice.assert_called_with(
            {
                'Name': 'vm',
                'IPAddress': '127.0.0.1',
                'HardwareAddress': '00:00:00:00:00:00',
                'Surname': 'fake',
                'SerialNumber': 'fakeserial',
                'Tag': 'mytag',
            }
        )

    def test_device_ipv6ready(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_ipv6ready(
            'vm',
            'true'
        )
        client.service.deviceUpdateIPv6Ready.assert_called_with(
            'vm', 'true')

    def test_device_create(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_create(
            'vm',
            location={
                'Floor': '0',
                'Room': '0',
                'Building': '0'
            },
            manufacturer='KVM',
            model='VIRTUAL MACHINE',
            os={
                'Name': 'LINUX',
                'Version': 'UNKNOWN'
            },
            responsible=TestClient.MANAGER_USER
        )
        client.service.vmCreate.assert_called_with(
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '', 'Tag': '',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'LandbManagerPerson': TestClient.MANAGER_USER,
                'UserPerson': None,
                'IPv6Ready': True,
                'ManagerLocked': True,
                'SerialNumber': ''
            }, {
                'VMParent': None
            }
        )

    def test_device_create_no_manager(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_create(
            'vm',
            location={
                'Floor': '0',
                'Room': '0',
                'Building': '0'
            },
            manufacturer='KVM',
            model='VIRTUAL MACHINE',
            os={
                'Name': 'LINUX',
                'Version': 'UNKNOWN'
            },
            responsible=TestClient.MANAGER_USER,
            manager_locked=False
        )
        client.service.vmCreate.assert_called_with(
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '', 'Tag': '',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': None,
                'LandbManagerPerson': None,
                'IPv6Ready': True,
                'ManagerLocked': False,
                'SerialNumber': ''
            }, {
                'VMParent': None
            }
        )

    def test_device_create_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmCreate.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.device_create(
                'vm',
                location={
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                manufacturer='KVM',
                model='VIRTUAL MACHINE',
                os={
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                responsible=TestClient.MANAGER_USER,
                manager_locked=False
            )
        client.service.vmCreate.assert_called_with(
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '', 'Tag': '',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': None,
                'LandbManagerPerson': None,
                'IPv6Ready': True,
                'ManagerLocked': False,
                'SerialNumber': ''
            }, {
                'VMParent': None
            }
        )

    def test_device_delete(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_delete(
            'vm', verify_no_interfaces=False)
        client.service.vmDestroy.assert_called_with('vm')

    def test_device_delete_fail(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmDestroy.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.device_delete(
                'vm', verify_no_interfaces=False)
        client.service.vmDestroy.assert_called_with('vm')

    def test_device_delete_fail_check(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceInfo.return_value = fixtures.DEVICE

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.device_delete(
                'vm', verify_no_interfaces=True)
        client.service.getDeviceInfo.assert_called_with('vm')

    def test_device_delete_with_check(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceInfo.return_value = fixtures.DEVICE_NO_INTF

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.device_delete(
            'vm', verify_no_interfaces=True)
        client.service.getDeviceInfo.assert_called_with('vm')
        client.service.vmDestroy.assert_called_with('vm')

    def test_vm_create(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_create(
            'vm',
            vm_location={
                'Floor': '0',
                'Room': '0',
                'Building': '0'
            },
            vm_manufacturer='KVM',
            vm_model='VIRTUAL MACHINE',
            vm_os={
                'Name': 'LINUX',
                'Version': 'UNKNOWN'
            },
            vm_responsible=TestClient.MANAGER_USER,
            vm_mac='00:00:00:00:00:00',
            vm_cluster='vmcluster',
            vm_parent='localhost'
        )
        client.service.vmCreate.assert_called_with(
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '', 'Tag': '',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': None,
                'LandbManagerPerson': None,
                'IPv6Ready': True,
                'ManagerLocked': False,
                'SerialNumber': ''
            }, {
                'VMParent': 'localhost'
            }
        )
        client.service.vmAddInterface.assert_called_with(
            'vm',
            'vm.cern.ch',
            'vmcluster', {
                'IP': '',
                'IPv6': '',
                'ServiceName': '',
                'InternetConnectivity': 'Y',
                'BindHardwareAddress':
                '00-00-00-00-00-00'
            }
        )
        client.service.vmAddCard.assert_called_with(
            'vm', {
                'HardwareAddress': '00:00:00:00:00:00',
                'CardType': 'Ethernet'
            }
        )

    def test_vm_create_with_alias(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_create(
            'vm',
            vm_location={
                'Floor': '0',
                'Room': '0',
                'Building': '0'
            },
            vm_manufacturer='KVM',
            vm_model='VIRTUAL MACHINE',
            vm_os={
                'Name': 'LINUX',
                'Version': 'UNKNOWN'
            },
            vm_responsible=TestClient.MANAGER_USER,
            vm_mac='00:00:00:00:00:00',
            vm_cluster='vmcluster',
            vm_parent='localhost',
            vm_alias=[
                'new-alias.cern.ch'
            ]
        )
        client.service.vmCreate.assert_called_with(
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '', 'Tag': '',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': None,
                'LandbManagerPerson': None,
                'IPv6Ready': True,
                'ManagerLocked': False,
                'SerialNumber': ''
            }, {
                'VMParent': 'localhost'
            }
        )
        client.service.vmAddInterface.assert_called_with(
            'vm',
            'vm.cern.ch',
            'vmcluster', {
                'IP': '',
                'IPv6': '',
                'ServiceName': '',
                'InternetConnectivity': 'Y',
                'BindHardwareAddress':
                '00-00-00-00-00-00'
            }
        )
        client.service.vmAddCard.assert_called_with(
            'vm', {
                'HardwareAddress': '00:00:00:00:00:00',
                'CardType': 'Ethernet'
            }
        )

    def test_vm_create_with_alias_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.interfaceAddAlias.side_effect = Exception("error")

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_create(
                'vm',
                vm_location={
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                vm_manufacturer='KVM',
                vm_model='VIRTUAL MACHINE',
                vm_os={
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                vm_responsible=TestClient.MANAGER_USER,
                vm_mac='00:00:00:00:00:00',
                vm_cluster='vmcluster',
                vm_parent='localhost',
                vm_alias=[
                    'new-alias.example.com'
                ]
            )
        client.service.vmCreate.assert_called_with(
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '', 'Tag': '',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': None,
                'LandbManagerPerson': None,
                'IPv6Ready': True,
                'ManagerLocked': False,
                'SerialNumber': ''
            }, {
                'VMParent': 'localhost'
            }
        )
        client.service.vmAddInterface.assert_called_with(
            'vm',
            'vm.cern.ch',
            'vmcluster', {
                'IP': '',
                'IPv6': '',
                'ServiceName': '',
                'InternetConnectivity': 'Y',
                'BindHardwareAddress':
                '00-00-00-00-00-00'
            }
        )
        client.service.vmAddCard.assert_called_with(
            'vm', {
                'HardwareAddress': '00:00:00:00:00:00',
                'CardType': 'Ethernet'
            }
        )

    def test_vm_create_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmCreate.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_create(
                'vm',
                vm_location={
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                vm_manufacturer='KVM',
                vm_model='VIRTUAL MACHINE',
                vm_os={
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                vm_responsible=TestClient.MANAGER_USER,
                vm_mac='00:00:00:00:00:00',
                vm_cluster='vmcluster',
                vm_parent='localhost'
            )
        client.service.vmCreate.assert_called_with(
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '', 'Tag': '',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': None,
                'LandbManagerPerson': None,
                'IPv6Ready': True,
                'ManagerLocked': False,
                'SerialNumber': ''
            }, {
                'VMParent': 'localhost'
            }
        )
        client.service.vmAddInterface.assert_not_called()
        client.service.vmAddCard.assert_not_called()

    def test_vm_create_fails_interface(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmAddInterface.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_create(
                'vm',
                vm_location={
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                vm_manufacturer='KVM',
                vm_model='VIRTUAL MACHINE',
                vm_os={
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                vm_responsible=TestClient.MANAGER_USER,
                vm_mac='00:00:00:00:00:00',
                vm_cluster='vmcluster',
                vm_parent='localhost'
            )
        client.service.vmCreate.assert_called_with(
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '', 'Tag': '',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': None,
                'LandbManagerPerson': None,
                'IPv6Ready': True,
                'ManagerLocked': False,
                'SerialNumber': ''
            }, {
                'VMParent': 'localhost'
            }
        )
        client.service.vmAddInterface.asswer_called_with(
            'vm',
            'vm.cern.ch',
            'vmcluster', {
                'IP': '',
                'IPv6': '',
                'ServiceName': '',
                'InternetConnectivity': 'Y',
                'BindHardwareAddress':
                '00-00-00-00-00-00'
            }
        )
        client.service.vmDestroy.assert_called_with('vm')
        client.service.vmAddCard.assert_called_with(
            'vm',
            {
                'HardwareAddress': '00:00:00:00:00:00',
                'CardType': 'Ethernet'
            }
        )

    def test_vm_add_interface(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_add_interface(
            'vm',
            'vmcluster',
            '00:00:00:00:00:00',
            '127.0.0.1',
            'fe90::1',
            'subnet',
        )
        client.service.vmAddInterface.assert_called_with(
            'vm',
            'Ethernet',
            'vmcluster',
            {
                'IP': '127.0.0.1',
                'IPv6': 'fe90::1',
                'ServiceName': 'subnet',
                'InternetConnectivity': 'Y',
                'BindHardwareAddress': '00-00-00-00-00-00'
            }
        )
        client.service.vmAddCard.assert_called_with(
            'vm',
            {
                'HardwareAddress': '00:00:00:00:00:00',
                'CardType': 'Ethernet'
            }
        )

    def test_vm_add_interface_no_internet(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_add_interface(
            'vm',
            'vmcluster',
            vm_mac='00:00:00:00:00:00',
            vm_ip='127.0.0.1',
            vm_ipv6='fe90::1',
            vm_ipservice='subnet',
            vm_internet=False
        )
        client.service.vmAddInterface.assert_called_with(
            'vm',
            'Ethernet',
            'vmcluster',
            {
                'IP': '127.0.0.1',
                'IPv6': 'fe90::1',
                'ServiceName': 'subnet',
                'InternetConnectivity': 'N',
                'BindHardwareAddress': '00-00-00-00-00-00'
            }
        )
        client.service.vmAddCard.assert_called_with(
            'vm',
            {
                'HardwareAddress': '00:00:00:00:00:00',
                'CardType': 'Ethernet'
            }
        )

    def test_vm_add_interface_interface_only(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_add_interface(
            'vm',
            'vmcluster',
            vm_mac='00:00:00:00:00:00',
            vm_ip='127.0.0.1',
            vm_ipv6='fe90::1',
            vm_ipservice='subnet',
            interface_only=True
        )
        client.service.vmAddInterface.assert_called_with(
            'vm',
            'Ethernet',
            'vmcluster',
            {
                'IP': '127.0.0.1',
                'IPv6': 'fe90::1',
                'ServiceName': 'subnet',
                'InternetConnectivity': 'Y',
                'BindHardwareAddress': '00-00-00-00-00-00'
            }
        )
        client.service.vmAddCard.assert_not_called()

    def test_vm_add_interface_interface_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.vmAddInterface.return_value = None
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_add_interface(
                'vm',
                'vmcluster',
                vm_mac='00:00:00:00:00:00',
                vm_ip='127.0.0.1',
                vm_ipv6='fe90::1',
                vm_ipservice='subnet',
                interface_only=True
            )
        client.service.vmAddInterface.assert_called_with(
            'vm',
            'Ethernet',
            'vmcluster',
            {
                'IP': '127.0.0.1',
                'IPv6': 'fe90::1',
                'ServiceName': 'subnet',
                'InternetConnectivity': 'Y',
                'BindHardwareAddress': '00-00-00-00-00-00'
            }
        )
        client.service.vmAddCard.assert_not_called()

    def test_vm_remove_interface(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.vmRemoveInterface.return_value = 'fake'
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_remove_interface(
            'vm',
            'interface',
            vm_mac='00:00:00:00:00:00',
        )
        client.service.vmRemoveInterface.assert_called_with(
            'vm',
            'interface'
        )
        client.service.vmRemoveCard.assert_called_with(
            'vm',
            '00:00:00:00:00:00',
        )

    def test_vm_remove_interface_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.vmRemoveInterface.return_value = None
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_remove_interface(
                'vm',
                'interface',
                vm_mac='00:00:00:00:00:00',
            )
        client.service.vmRemoveInterface.assert_called_with(
            'vm',
            'interface'
        )
        client.service.vmRemoveCard.assert_not_called()

    def test_vm_remove_interface_fails_card(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.vmRemoveInterface.return_value = 'fake'
        client.service.vmRemoveCard.return_value = None
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_remove_interface(
                'vm',
                'interface',
                vm_mac='00:00:00:00:00:00',
            )
        client.service.vmRemoveInterface.assert_called_with(
            'vm',
            'interface'
        )
        client.service.vmRemoveCard.assert_called_with(
            'vm',
            '00:00:00:00:00:00',
        )

    def test_bind_unbind_interface(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.bindUnbindInterface.return_value = 'fake'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.bind_unbind_interface(
            'vm',
            '00:00:00:00:00:00'
        )
        client.service.bindUnbindInterface.assert_called_with(
            'vm.CERN.CH',
            '00:00:00:00:00:00'
        )

    def test_bind_unbind_interface_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.bindUnbindInterface.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.bind_unbind_interface(
                'vm',
                '00:00:00:00:00:00'
            )
        client.service.bindUnbindInterface.assert_called_with(
            'vm.CERN.CH',
            '00:00:00:00:00:00'
        )

    def test_vm_add_card(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmAddCard.return_value = 'fake'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_add_card(
            'vm',
            '00:00:00:00:00:00'
        )
        client.service.vmAddCard.assert_called_with(
            'vm',
            {
                'HardwareAddress': '00:00:00:00:00:00',
                'CardType': 'Ethernet'
            }
        )

    def test_vm_add_card_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmAddCard.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_add_card(
                'vm',
                '00:00:00:00:00:00'
            )
        client.service.vmAddCard.assert_called_with(
            'vm',
            {
                'HardwareAddress': '00:00:00:00:00:00',
                'CardType': 'Ethernet'
            }
        )

    def test_vm_remove_card(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmRemoveCard.return_value = 'fake'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_remove_card(
            'vm',
            '00:00:00:00:00:00'
        )
        client.service.vmRemoveCard.assert_called_with(
            'vm',
            '00:00:00:00:00:00'
        )

    def test_vm_remove_card_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmRemoveCard.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_remove_card(
                'vm',
                '00:00:00:00:00:00'
            )
        client.service.vmRemoveCard.assert_called_with(
            'vm',
            '00:00:00:00:00:00'
        )

    def test_vm_update(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceBasicInfo.return_value.IPv6Ready = 'Y'
        client.service.getDeviceBasicInfo.return_value.Description = ''
        client.service.getDeviceBasicInfo.return_value.OperatingSystem = {
            'Name': 'UNKNOWN',
            'Version': 'UNKNOWN'
        }
        client.service.getDeviceBasicInfo.return_value.ResponsiblePerson = (
            TestClient.MANAGER_USER
        )
        client.service.getDeviceBasicInfo.return_value.UserPerson = (
            TestClient.MANAGER_USER
        )
        client.service.getDeviceBasicInfo.return_value.LandbManagerPerson = (
            TestClient.MANAGER_USER
        )
        client.service.getDeviceBasicInfo.return_value.ManagerLocked = 'Y'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_update(
            'vm',
        )
        client.service.vmUpdate.assert_called_with(
            'vm',
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': '',
                'Tag': 'OPENSTACK VM',
                'OperatingSystem': {
                    'Name': 'UNKNOWN',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': TestClient.MANAGER_USER,
                'IPv6Ready': 'Y',
                'LandbManagerPerson': TestClient.MANAGER_USER,
                'ManagerLocked': 'Y'
            }
        )

    def test_vm_update_fails_retrieval(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceBasicInfo.side_effect = (
            Exception('error'))

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_update(
            'vm',
            ipv6ready='N'
        )
        client.service.vmUpdate.assert_called_with(
            'vm',
            {
                'DeviceName': 'vm',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': None,
                'Tag': 'OPENSTACK VM',
                'OperatingSystem': None,
                'ResponsiblePerson': None,
                'UserPerson': None,
                'IPv6Ready': 'N',
                'LandbManagerPerson': None,
                'ManagerLocked': None
            }
        )

    def test_vm_delete(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_delete(
            'vm',
        )
        client.service.vmDestroy.assert_called_with(
            'vm',
        )

    def test_vm_delete_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmDestroy.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.vm_delete(
                'vm',
            )
        client.service.vmDestroy.assert_called_with(
            'vm',
        )

    def test_vm_delete_rename(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceBasicInfo.return_value.IPv6Ready = 'Y'
        client.service.getDeviceBasicInfo.return_value.LandbManagerPerson = (
            TestClient.MANAGER_USER
        )
        client.service.getDeviceBasicInfo.return_value.ManagerLocked = 'Y'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_delete(
            'vm',
            destroy=False,
            new_device='pXX'
        )
        client.service.vmDestroy.assert_not_called()
        client.service.vmUpdate.assert_called_with(
            'vm',
            {
                'DeviceName': 'pXX',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Manufacturer': 'KVM',
                'Model': 'VIRTUAL MACHINE',
                'Description': 'Not in use',
                'Tag': 'OPENSTACK VM',
                'OperatingSystem': {
                    'Name': 'UNKNOWN',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': TestClient.MANAGER_USER,
                'IPv6Ready': 'Y',
                'LandbManagerPerson': TestClient.MANAGER_USER,
                'ManagerLocked': 'Y'
            }
        )

    def test_vm_delete_random(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceInfo.return_value = None
        client.service.getDeviceBasicInfo.return_value.IPv6Ready = 'Y'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_delete(
            'vm',
            destroy=False,
        )
        client.service.vmDestroy.assert_not_called()

    def test_vm_migrate(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_migrate(
            'vm',
            'hypervisor'
        )
        client.service.vmMigrate.assert_called_with(
            'vm',
            'hypervisor'
        )

    def test_cluster_devices(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmClusterGetDevices.return_value = [
            'vm1', 'vm2']

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.cluster_devices(
            'vmcluster'
        )
        client.service.vmClusterGetDevices.assert_called_with(
            'vmcluster'
        )
        client.service.getDeviceInfoArray.assert_not_called()

    def test_cluster_devices_detailed(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmClusterGetDevices.return_value = [
            'vm1']
        client.service.getDeviceInfoArray.return_value = [
            mock.MagicMock()
        ]

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.cluster_devices(
            'vmcluster',
            detail=True,
        )
        client.service.vmClusterGetDevices.assert_called_with(
            'vmcluster'
        )
        client.service.getDeviceInfoArray.assert_has_calls([
            mock.call(['vm1'])
        ])

    def test_cluster_info_by_cluster(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.cluster_info(
            cluster='vmcluster',
        )
        client.service.vmClusterGetInfo.assert_called_with(
            'vmcluster'
        )

    def test_cluster_info_by_cluster_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.cluster_info(
                cluster=None,
            )
        client.service.vmClusterGetInfo.assert_not_called()

    def test_cluster_info_by_device(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmGetClusterMembership.return_value = [
            'vmcluster2'
        ]
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.cluster_info(
            device='vm1',
        )

        client.service.vmGetClusterMembership.assert_called_with(
            'vm1'
        )

        client.service.vmClusterGetInfo.assert_called_with(
            'vmcluster2'
        )

    def test_cluster_info_by_device_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmGetClusterMembership.return_value = [
            None
        ]
        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.cluster_info(
                device='vm1',
            )

        client.service.vmGetClusterMembership.assert_called_with(
            'vm1'
        )

        client.service.vmClusterGetInfo.assert_not_called()

    def test_ipservice_register(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.ipservice_register(
            ipservice='subnet',
            parent='localhost',
        )

    def test_ipservice_info(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.ipservice_info(
            'subnet'
        )
        client.service.getServiceInfo.assert_called_with(
            'subnet'
        )

    def test_ipservice_devices(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.ipservice_devices(
            'subnet'
        )
        client.service.getDevicesFromService.assert_called_with(
            'subnet'
        )

    def test_alias_update(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.side_effect = iter([
            ['device'],
            None
        ])
        client.service.getDeviceInfo.return_value = (
            fixtures.DEVICE
        )

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.alias_update(
            'interface.cern.ch',
            ['new-alias.cern.ch']
        )

        client.service.searchDevice.assert_has_calls([
            mock.call({'Name': 'interface.cern.ch'}),
            mock.call({
                'Name': 'NEW-ALIAS.CERN.CH',
                'IPAddress': '',
                'HardwareAddress': '',
                'Surname': '',
                'SerialNumber': '',
                'Tag': ''
            })
        ])

    def test_alias_update_not_found(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.return_value = []

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.alias_update(
            'interface.cern.ch',
            ['new-alias.cern.ch']
        )

        client.service.searchDevice.assert_has_calls([
            mock.call({'Name': 'interface.cern.ch'}),
        ])

    def test_alias_update_fail_to_verify(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.return_value = [
            fixtures.DEVICE_NO_ALIAS
        ]

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.alias_update(
            'interface-new.cern.ch',
            ['new-alias.cern.ch']
        )

        client.service.searchDevice.assert_has_calls([
            mock.call({'Name': 'interface-new.cern.ch'}),
        ])

    def test_alias_update_no_old_alias(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.side_effect = iter([
            [fixtures.DEVICE_NO_ALIAS],
            None
        ])
        client.service.getDeviceInfo.return_value = (
            fixtures.DEVICE_NO_ALIAS
        )

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.alias_update(
            'interface.cern.ch',
            ['new-alias.cern.ch']
        )

        client.service.searchDevice.assert_has_calls([
            mock.call({'Name': 'interface.cern.ch'}),
            mock.call({
                'Name': 'NEW-ALIAS.CERN.CH',
                'IPAddress': '',
                'HardwareAddress': '',
                'Surname': '',
                'SerialNumber': '',
                'Tag': ''
            })
        ])

    def test_alias_update_wrong_domain(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.side_effect = iter([
            [fixtures.DEVICE],
            None
        ])
        client.service.getDeviceInfo.return_value = (
            fixtures.DEVICE
        )

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.alias_update(
            'interface.cern.ch',
            ['new-alias.example.com']
        )

        client.service.searchDevice.assert_has_calls([
            mock.call({'Name': 'interface.cern.ch'})
        ])

    def test_alias_update_already_exists(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.side_effect = iter([
            [fixtures.DEVICE],
            [fixtures.DEVICE],
        ])
        client.service.getDeviceInfo.return_value = (
            fixtures.DEVICE
        )

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.alias_update(
            'interface.cern.ch',
            ['new-alias.cern.ch']
        )

        client.service.searchDevice.assert_has_calls([
            mock.call({'Name': 'interface.cern.ch'})
        ])

    def test_alias_update_set_exception(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.side_effect = iter([
            [fixtures.DEVICE],
            None,
            [fixtures.DEVICE]
        ])
        client.service.interfaceAddAlias.side_effect = iter([
            Exception('error'),
            None
        ])
        client.service.getDeviceInfo.return_value = (
            fixtures.DEVICE
        )

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.alias_update(
            'interface.cern.ch',
            ['new-alias.cern.ch']
        )

        client.service.searchDevice.assert_has_calls([
            mock.call({'Name': 'interface.cern.ch'})
        ])

    def test_alias_update_remove_exception(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.searchDevice.side_effect = iter([
            [fixtures.DEVICE],
            None,
            [fixtures.DEVICE]
        ])
        client.service.interfaceRemoveAlias.side_effect = iter([
            Exception('error'),
            None
        ])
        client.service.getDeviceInfo.return_value = (
            fixtures.DEVICE
        )

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.alias_update(
            'interface.cern.ch',
            ['new-alias.cern.ch']
        )

        client.service.searchDevice.assert_has_calls([
            mock.call({'Name': 'interface.cern.ch'})
        ])

    def test_vm_get_cluster_membership(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.vm_get_cluster_membership(
            'mynode.cern.ch'
        )
        client.service.vmGetClusterMembership.assert_called_with(
            'mynode'
        )

    def test_service_info_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmClusterGetInfo.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80'
        )
        with self.assertRaises(Exception):
            landb.service_info(
                'vmcluster',
            )
        client.service.vmClusterGetInfo.assert_called_with(
            'vmcluster',
        )

    def test_service_info_no_services(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmClusterGetInfo.return_value = {
            'Services': None
        }

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80'
        )
        with self.assertRaises(Exception):
            landb.service_info(
                'vmcluster',
            )
        client.service.vmClusterGetInfo.assert_called_with(
            'vmcluster',
        )

    def test_service_info(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.vmClusterGetInfo.return_value = {
            'Name': 'vmcluster',
            'Services': ['subnet1']
        }

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80'
        )
        landb.service_info(
            'vmcluster',
        )
        client.service.vmClusterGetInfo.assert_called_with(
            'vmcluster',
        )
        client.service.getServiceInfo.assert_called_with(
            'subnet1'
        )

    def test_get_setinfo(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getSetInfo.return_value = 'fake'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.get_setinfo(
            'MYSET'
        )
        client.service.getSetInfo.assert_called_with(
            'myset'
        )

    def test_get_setinfo_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getSetInfo.return_value = None

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.get_setinfo(
                'MYSET'
            )
        client.service.getSetInfo.assert_called_with(
            'myset'
        )

    def test_get_addresses_set(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getSetInfo.return_value = fixtures.SET
        client.service.getSetInfo.return_value.Addresses = [
            'INTF1', 'INTF2'
        ]

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        res = landb.get_addresses_set(
            'MYSET'
        )
        client.service.getSetInfo.assert_called_with(
            'myset'
        )
        assert res == ['INTF1', 'INTF2']

    def test_update_address_empty_description(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getSetInfo.return_value = ''

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.update_address_set(
                'MYSET',
                'uuid',
                'localhost'
            )

        client.service.getSetInfo.assert_called_with('myset')

    def test_update_address_set_fails(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getSetInfo.return_value = fixtures.SET_EMPTY

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.update_address_set(
                'MYSET',
                'uuid',
                'localhost'
            )

        client.service.getSetInfo.assert_called_with('myset')

    def test_update_address_wrong_description(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getSetInfo.return_value = fixtures.SET_WRONG

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.update_address_set(
                'MYSET',
                'uuid',
                'localhost'
            )

        client.service.getSetInfo.assert_called_with('myset')

    def test_update_address_exception(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getSetInfo.return_value = fixtures.SET
        client.service.setInsertAddress.side_effect = Exception("error")

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.update_address_set(
                'MYSET',
                'uuid',
                'localhost'
            )

        client.service.getSetInfo.assert_called_with('myset')

    def test_update_address(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getSetInfo.return_value = fixtures.SET

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        landb.update_address_set(
            'MYSET',
            'uuid',
            'localhost'
        )

        client.service.getSetInfo.assert_called_with('myset')

    def test_delete_address_set(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')
        landb.delete_address_set('TESTSET', 'INTERFACE')

        client.service.setDeleteAddress.assert_called_with('TESTSET',
                                                           'INTERFACE')

    def test_device_rename_ironic(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        client.service.getDeviceInfo.return_value = fixtures.DEVICE_IRONIC
        client.service.getDeviceInfo.return_value.Location = {
            'Floor': '0',
            'Room': '0',
            'Building': '0'
        }
        client.service.getDeviceInfo.return_value.Model = "model"
        client.service.getDeviceInfo.return_value.Manufacturer = "manuf"
        client.service.getDeviceInfo.return_value.Zone = "ABC"
        client.service.getDeviceInfo.return_value.SerialNumber = "dl1234"
        client.service.getDeviceInfo.return_value.LandbManagerPerson = (
            TestClient.MANAGER_USER
        )
        client.service.getDeviceInfo.return_value.ManagerLocked = 'Y'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')
        landb.device_rename_ironic('node', 'new_name')

        client.service.deviceUpdate.assert_called_with(
            'node',
            {
                'DeviceName': 'new_name',
                'Location': {
                    'Floor': '0',
                    'Room': '0',
                    'Building': '0'
                },
                'Zone': 'ABC',
                'Manufacturer': 'manuf',
                'Model': 'model',
                'SerialNumber': 'dl1234',
                'Description': "Not in use",
                'Tag': 'OPENSTACK IRONIC NODE',
                'OperatingSystem': {
                    'Name': 'LINUX',
                    'Version': 'UNKNOWN'
                },
                'ResponsiblePerson': TestClient.MANAGER_USER,
                'UserPerson': TestClient.MANAGER_USER,
                'LandbManagerPerson': TestClient.MANAGER_USER,
                'IPv6Ready': True,
                'HCPResponse': 'true'
            }
        )

    def test_interface_rename(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')
        landb.interface_rename('abc', 'def')

        client.service.interfaceRename.assert_called_with('abc', 'def')

    def test_isIronic_no(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceBasicInfo.return_value = fixtures.DEVICE

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        assert landb.isIronic('localhost') is False

    def test_isIronic_yes(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceBasicInfo.return_value = fixtures.DEVICE_IRONIC

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        assert landb.isIronic('localhost') is True

    def test_isIronic_not_found(self, mock_landb):
        client = mock_landb.return_value
        client.service.getAuthToken.return_value = 'token'
        client.service.getDeviceBasicInfo.side_effect = (
            Exception('error'))

        landb = LanDB(
            username='user',
            password='password',
            host='localhost',
            port='80')

        with self.assertRaises(Exception):
            landb.isIronic(
                'localhost'
            )
