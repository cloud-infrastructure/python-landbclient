"""Landb functional test suite."""
import logging
import os

from unittest import TestCase

from landbclient.client import LanDB


def _get_vm(
        i, cluster='defaultcluster', ipservice='defaultipsrv',
        ip_addr='127.0.0.1', mac='ff:ff:ff:ff:ff:ff', ipv6='::0',
        ipv6ready=True, manager_locked='', vm_internet=True,
        vm_domain_name='cern.ch', alias='alias', serialnumber='no123456',
        tag="vm-default-tag"):
    return {
        'DeviceName': f"{i}",
        'Location': {
            'Floor': f"vm-{i}-floor",
            'Building': f"vm-{i}-building",
            'Room': f"vm-{i}-room"
        },
        'Manufacturer': f"vm-{i}-manufacturer",
        'Model': f"vm-{i}-model",
        'Tag': tag,
        'SerialNumber': serialnumber,
        'OperatingSystem': {
            'Name': f"vm-{i}-osname",
            'Version': f"vm-{i}-osversion"
        },
        'ResponsiblePerson': {
            'Name': f"vm-{i}-respname",
            'FirstName': f"vm-{i}-respfirstname"
        },
        'UserPerson': {
            'Name': f"vm-{i}-username",
            'FirstName': f"vm-{i}-userfirstname"
        },
        'LandbManagerPerson': {
            'Name': f"vm-{i}-respname",
            'FirstName': f"vm-{i}-respfirstname"
        },
        'mac': mac,
        'Cluster': cluster,
        'Parent': f"vm-{i}-parent",
        'Description': f"vm-{i}-description",
        'Interfaces': [
            {
                'ServiceName': ipservice,
                'IPAddress': ip_addr,
                'IPv6Address': ipv6,
                'IPAliases': alias
            }
        ],
        'ipservice': ipservice,
        'IPv6Ready': ipv6ready,
        'ManagerLocked': manager_locked,
        'Internet': vm_internet,
        'DomainName': vm_domain_name,
    }


class TestClient(TestCase):
    """Landb client tests."""

    def setUp(self):
        logging.getLogger('suds').setLevel(logging.INFO)
        logging.captureWarnings(True)
        host = "localhost"
        if "LANDB_HOST" in os.environ:
            host = os.environ['LANDB_HOST']

        self.landb = LanDB(
            "test", "123456", host, "8083", None, None, None, "http")

    def test_vm_create(self):
        """Test for vm creation."""
        vm_test = _get_vm('create')
        self.landb.vm_create(
            vm_test['DeviceName'], vm_test['Location'],
            vm_test['Manufacturer'],
            vm_test['Model'], vm_test['OperatingSystem'],
            vm_test['ResponsiblePerson'],
            vm_test['mac'], vm_test['Cluster'], vm_test['Parent'],
            vm_test['Description'],
            vm_test['Tag'], vm_test['UserPerson'],
            vm_test['Interfaces'][0]['IPAddress'],
            vm_test['Interfaces'][0]['IPv6Address'],
            vm_test['ipservice'],
            vm_test['Interfaces'][0]['IPAliases']
        )
        result = self.landb.device_info(vm_test['DeviceName'])
        self._assert_vm(result, vm_test)

    def test_vm_create_withpersonid(self):
        """Test vm creation with a valid person."""
        vm_test = _get_vm('create')
        vm_test['ResponsiblePerson'] = {'PersonID': 0}
        vm_test['UserPerson'] = {'PersonID': 0}
        self.landb.vm_create(
            vm_test['DeviceName'], vm_test['Location'],
            vm_test['Manufacturer'],
            vm_test['Model'], vm_test['OperatingSystem'],
            vm_test['ResponsiblePerson'],
            vm_test['mac'], vm_test['Cluster'], vm_test['Parent'],
            vm_test['Description'],
            vm_test['Tag'], vm_test['UserPerson'],
            vm_test['Interfaces'][0]['IPAddress'],
            vm_test['Interfaces'][0]['IPv6Address'],
            vm_test['ipservice'],
        )
        vm_test['ResponsiblePerson'] = {'CCID': 0}
        vm_test['UserPerson'] = {'CCID': 0}
        result = self.landb.device_info(vm_test['DeviceName'])
        self._assert_vm(result, vm_test)

    def test_vm_delete(self):
        """Test vm deletion."""
        vm_test = _get_vm('delete')
        self._create_vm(vm_test)
        vm_result = self.landb.device_info(vm_test['DeviceName'])
        self.assertIsNotNone(vm_result)
        self.landb.vm_delete(vm_test['DeviceName'])
        with self.assertRaises(Exception):
            self.landb.device_info(vm_test['DeviceName'])

    def test_device_exists(self):
        """Test device exists."""
        vm_test = _get_vm('exists')
        self._create_vm(vm_test)
        result = self.landb.device_exists(vm_test['DeviceName'])
        self.assertTrue(result)

    def test_device_not_exists(self):
        """Test device does not exist."""
        result = self.landb.device_exists('will-never-exist-vmname')
        self.assertFalse(result)

    def test_device_exists_none(self):
        """Test device exists with a None input."""
        result = self.landb.device_exists(None)
        self.assertFalse(result)

    def test_device_info(self):
        """Test retrival of device info."""
        vm_test = _get_vm('info')
        self._create_vm(vm_test)
        result = self.landb.device_info(vm_test['DeviceName'])
        self._assert_vm(result, vm_test)

    def test_device_info_notexists(self):
        """Test retrieval of device info for non existing device."""
        with self.assertRaises(Exception):
            self.landb.device_info('will-never-exist-vmname')

    def test_device_info_none(self):
        """Test retrieval of device info passing None device."""
        with self.assertRaises(Exception):
            self.landb.device_info(None)

    def test_device_managed(self):
        """Test setting device as managed."""
        # TODO(isantosp)
        pass

    def test_device_hostname(self):
        """Test device hostname retrieval from IP."""
        vm_test = _get_vm('hostname', ip_addr='10.10.10.1')
        self._create_vm(vm_test)
        result = self.landb.device_hostname('10.10.10.1')
        self.assertIsNotNone(result)
        self.assertEqual(result[0], vm_test['DeviceName'])

    def test_device_hostname_none(self):
        """Test device hostname retrieval from IP when output is None."""
        result = self.landb.device_hostname('0.0.0.0')
        self.assertIsNone(result)

    def test_device_search_ip(self):
        """Test device hostname search using ip."""
        vm_test = _get_vm('search-ip', ip_addr='10.10.10.2')
        self._create_vm(vm_test)
        result = self.landb.device_search(ip_addr='10.10.10.2')
        self.assertIsNotNone(result)
        self.assertEqual(result[0], vm_test['DeviceName'])

    def test_device_search_ip_none(self):
        """Test device hostname search using None ip."""
        with self.assertRaises(Exception):
            self.landb.device_search(ip_addr=None)

    def test_device_search_mac(self):
        """Test device hostname search using mac address."""
        vm_test = _get_vm('search-mac', mac='ff:ff:ff:ff:ff:01')
        self._create_vm(vm_test)
        result = self.landb.device_search(mac='ff:ff:ff:ff:ff:01')
        self.assertIsNotNone(result)
        self.assertEqual(result[0], vm_test['DeviceName'])

    def test_device_search_mac_none(self):
        """Test device hostname search using None mac."""
        with self.assertRaises(Exception):
            self.landb.device_search(mac=None)

    def test_device_search_serialnumber(self):
        """Test device hostname search using serial number."""
        vm_test = _get_vm('search-serialnumber', serialnumber='no-12345')
        self._create_vm(vm_test)
        result = self.landb.device_search(serialnumber='no-12345')
        self.assertIsNotNone(result)
        self.assertEqual(result[0], vm_test['DeviceName'])

    def test_device_search_serialnumber_none(self):
        """Test device hostname search using None serial number."""
        with self.assertRaises(Exception):
            self.landb.device_search(serialnumber=None)

    def test_device_search_tag(self):
        """Test device hostname search using node tag."""
        vm_test = _get_vm('search-tag', tag='Bitcoin ring')
        self._create_vm(vm_test)
        result = self.landb.device_search(tag='Bitcoin ring')
        self.assertIsNotNone(result)
        self.assertEqual(result[0], vm_test['DeviceName'])

    def test_device_search_tag_none(self):
        """Test device hostname search using None node tag."""
        with self.assertRaises(Exception):
            self.landb.device_search(tag=None)

    def test_device_search_surname(self):
        """Test device hostname search using username."""
        vm_test = _get_vm('search-surname')
        self.landb.vm_create(
            vm_test['DeviceName'], vm_test['Location'],
            vm_test['Manufacturer'],
            vm_test['Model'], vm_test['OperatingSystem'],
            vm_test['ResponsiblePerson'],
            vm_test['mac'], vm_test['Cluster'], vm_test['Parent'])
        result = self.landb.device_search(
            surname=vm_test['ResponsiblePerson']['Name'])
        self.assertIsNotNone(result)
        self.assertEqual(result[0], vm_test['DeviceName'])

    def test_device_search_surname_none(self):
        """Test device hostname search using None surname."""
        with self.assertRaises(Exception):
            self.landb.device_search(surname=None)

    def test_vm_get_cluster_membership(self):
        """Test retrieval of a device cluster membership."""
        vm_test = _get_vm(
            'cluster-membership', cluster='cluster-membership',
            ipservice='cluster_membership_ipservice')
        self._create_vm(vm_test)
        cluster = self.landb.vm_get_cluster_membership(vm_test['Cluster'])
        self.assertEqual(len(cluster), 1)
        self.assertEqual(cluster[0], 'cluster-membership')

    def test_vm_create_withoutoptinfo(self):
        """Test vm creation without optional info."""
        vm_test = _get_vm('create-without-optional-info')
        self.landb.vm_create(
            vm_test['DeviceName'], vm_test['Location'],
            vm_test['Manufacturer'],
            vm_test['Model'], vm_test['OperatingSystem'],
            vm_test['ResponsiblePerson'],
            vm_test['mac'], vm_test['Cluster'], vm_test['Parent'])
        result = self.landb.device_info(vm_test['DeviceName'])
        self.assertEqual(result['DeviceName'], vm_test['DeviceName'])

    def test_vm_create_with_domain_name(self):
        """Test creation of a vm with a domain name."""
        vm_test = _get_vm('create-vm-with-domain-name', ip_addr='10.10.10.3',
                          vm_domain_name='dns.domain')
        self._create_vm(vm_test)
        result = self.landb.device_info(vm_test['DeviceName'])
        self.assertEqual(result['Interfaces'][0]['Name'],
                         'create-vm-with-domain-name.dns.domain')

    def test_add_interface(self):
        """Test adding an interface to an existing device."""
        vm_test = _get_vm('add-interface', ip_addr='10.10.10.3')
        self._create_vm(vm_test)
        func_ok = self.landb.vm_add_interface(
            vm_test['DeviceName'], vm_test['Cluster'],
            vm_mac='ff:ff:ff:ff:00:00', vm_ip='10.10.10.5',
            vm_ipservice='defaultipsrv1234')
        self.assertTrue(func_ok)
        result = self.landb.device_info(vm_test['DeviceName'])
        self.assertIsNotNone(result)
        self.assertEqual(result['DeviceName'], vm_test['DeviceName'])
        self.assertEqual(
            result['NetworkInterfaceCards'][1]['HardwareAddress'],
            'ff:ff:ff:ff:00:00')
        self.assertEqual(
            result['Interfaces'][1]['ServiceName'],
            'defaultipsrv1234')
        self.assertEqual(result['Interfaces'][1]['IPAddress'], '10.10.10.5')
        self.assertEqual(len(result['NetworkInterfaceCards']), 2)
        self.assertEqual(len(result['Interfaces']), 2)

    def test_remove_interface(self):
        """Test removing an interface from a device."""
        vm_test = _get_vm('remove-interface', ip_addr='10.10.10.3')
        self._create_vm(vm_test)
        func_ok = self.landb.vm_remove_interface(
            vm_test['DeviceName'],
            f"remove-interface.{vm_test['DomainName']}")
        self.assertTrue(func_ok)
        result = self.landb.device_info(vm_test['DeviceName'])
        self.assertEqual(len(result['Interfaces']), 0)

    def test_cluster_info_withvm(self):
        """Test retrieval of cluster info passing a vm."""
        vm_test = _get_vm(
            'cluster_info_vm',
            ipservice='cluster_info_vm_ipservice')
        self._create_vm(vm_test)
        result = self.landb.cluster_info(device='cluster_info_vm')
        self.assertEqual(
            result['Services'][(len(result['Services']) - 1)],
            'cluster_info_vm_ipservice')

    def test_cluster_info_withcluster(self):
        """Test retrieval of cluster info passing a cluster."""
        vm_test = _get_vm(
            'cluster_info_cluster', cluster='cluster_info_cluster_cluster',
            ipservice='cluster_info_cluster_ipservice')
        self._create_vm(vm_test)
        result = self.landb.cluster_info(
            cluster='cluster_info_cluster_cluster')
        self.assertEqual(result['Name'], 'cluster_info_cluster_cluster')
        self.assertEqual(
            result['Services'][0],
            'cluster_info_cluster_ipservice')

    def test_cluster_info_allnone(self):
        """Test retrieval of cluster info passing None cluster and device."""
        vm_test = _get_vm('cluster_info_allnone')
        self._create_vm(vm_test)
        with self.assertRaises(Exception):
            self.landb.service_info(cluster=None, device=None)

    def test_cluster_info_devicecluster(self):
        """Test retrieval of cluster info passing device and cluster."""
        vm_test = _get_vm(
            'cluster_info_device_and_cluster_host',
            cluster='cluster_info_device_and_cluster_cluster',
            ipservice='cluster_info_device_and_cluster_ipservice')
        self._create_vm(vm_test)
        result = self.landb.cluster_info(
            device='cluster_info_device_and_cluster_host',
            cluster='cluster_info_device_and_cluster_cluster')
        self.assertEqual(
            result['Name'],
            'cluster_info_device_and_cluster_cluster')

    def test_service_info_withvm(self):
        """Test retrieval of service info passing a vm."""
        vm_test = _get_vm(
            'service_info_vm',
            ipservice='service_info_vm_ipservice')
        self._create_vm(vm_test)
        result = self.landb.service_info(device='service_info_vm')
        self.assertEqual(
            result['services']['service_info_vm_ipservice']['Name'],
            'service_info_vm_ipservice')

    def test_service_info_withcluster(self):
        """Test retrieval of service info passing a cluster."""
        vm_test = _get_vm(
            'service_info_cluster', cluster='service_info_cluster_cluster',
            ipservice='service_info_cluster_ipservice')
        self._create_vm(vm_test)
        result = self.landb.service_info(
            cluster='service_info_cluster_cluster')
        self.assertEqual(result['cluster'], 'service_info_cluster_cluster')

    def test_service_info_allnone(self):
        """Test retrieval of service info passing cluster and device None."""
        vm_test = _get_vm('service_info_allnone')
        self._create_vm(vm_test)
        with self.assertRaises(Exception):
            self.landb.service_info(cluster=None, device=None)

    def _create_vm(self, vm_test):
        result = self.landb.vm_create(
            vm_test['DeviceName'],
            vm_test['Location'],
            vm_test['Manufacturer'],
            vm_test['Model'],
            vm_test['OperatingSystem'],
            vm_test['ResponsiblePerson'],
            vm_test['mac'],
            vm_test['Cluster'],
            vm_test['Parent'],
            vm_test['Description'],
            vm_test['Tag'],
            vm_test['UserPerson'],
            vm_test['Interfaces'][0]['IPAddress'],
            vm_test['Interfaces'][0]['IPv6Address'],
            vm_test['ipservice'],
            vm_test['Internet'],
            vm_test['LandbManagerPerson'],
            vm_test['IPv6Ready'],
            vm_test['ManagerLocked'],
            vm_domain_name=vm_test['DomainName'],
            vm_serialnumber=vm_test['SerialNumber']
        )
        self.assertTrue(result)

    def _assert_vm(self, result, expected):
        self.assertEqual(result['DeviceName'], expected['DeviceName'])
        self.assertEqual(
            result['Location']['Building'],
            expected['Location']['Building'])
        self.assertEqual(
            result['Location']['Floor'],
            expected['Location']['Floor'])
        self.assertEqual(
            result['Location']['Room'],
            expected['Location']['Room'])
        self.assertEqual(result['Manufacturer'], expected['Manufacturer'])
        self.assertEqual(result['Model'], expected['Model'])
        self.assertEqual(
            result['OperatingSystem']['Name'],
            expected['OperatingSystem']['Name'])
        self.assertEqual(
            result['OperatingSystem']['Version'],
            expected['OperatingSystem']['Version'])
        if 'CCID' in expected['ResponsiblePerson']:
            self.assertEqual(
                result['ResponsiblePerson']['CCID'],
                expected['ResponsiblePerson']['CCID'])
        if 'FirstName' in expected['ResponsiblePerson']:
            self.assertEqual(
                result['ResponsiblePerson']['FirstName'],
                expected['ResponsiblePerson']['FirstName'])
        if 'Name' in expected['ResponsiblePerson']:
            self.assertEqual(
                result['ResponsiblePerson']['Name'],
                expected['ResponsiblePerson']['Name'])
        if 'CCID' in expected['UserPerson']:
            self.assertEqual(
                result['UserPerson']['CCID'],
                expected['UserPerson']['CCID'])
        if 'FirstName' in expected['UserPerson']:
            self.assertEqual(
                result['UserPerson']['FirstName'],
                expected['UserPerson']['FirstName'])
        if 'Name' in expected['UserPerson']:
            self.assertEqual(
                result['UserPerson']['Name'],
                expected['UserPerson']['Name'])
        self.assertEqual(
            result['Interfaces'][0]['ServiceName'],
            expected['Interfaces'][0]['ServiceName'])
        self.assertEqual(
            result['Interfaces'][0]['IPAddress'],
            expected['Interfaces'][0]['IPAddress'])
