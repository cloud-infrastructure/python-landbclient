python-landbclient
==================

[![pipeline status](https://gitlab.cern.ch/cloud-infrastructure/python-landbclient/badges/master/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/python-landbclient/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cloud-infrastructure/python-landbclient/badges/master/coverage.svg)](https://gitlab.cern.ch/cloud-infrastructure/python-landbclient/-/commits/master)


## Overview

Command line tool and python library to access and manage information in CERN's landb.

Sample usage:
```
# landb --landb-user <username> dev-exists cci-jenkins
```

Getting help on the commands:
```
# landb --help
usage: landb [--version] [-v] [--log-file LOG_FILE] [-q] [-h] [--debug]
             [--landb-user LANDB_USER] [--landb-password LANDB_PASSWORD]
             [--landb-host LANDB_HOST] [--landb-port LANDB_PORT]
             [--cert-file CERT_FILE] [--key-file KEY_FILE] [--ca-file CA_FILE]
             [--protocol PROTOCOL] [--trace]

Client for interacting with the CERN landb.

optional arguments:
  --version             show program's version number and exit
  -v, --verbose         Increase verbosity of output. Can be repeated.
  --log-file LOG_FILE   Specify a file to log output. Disabled by default.
  -q, --quiet           suppress output except warnings and errors
  -h, --help            show this help message and exit
  --debug               show tracebacks on errors
  --landb-user LANDB_USER
                        landb username
  --landb-password LANDB_PASSWORD
                        landb password
  --landb-host LANDB_HOST
                        landb host (default network.cern.ch)
  --landb-port LANDB_PORT
                        landb port (default 443)
  --cert-file CERT_FILE
                        certificate to use
  --key-file KEY_FILE   key to use
  --ca-file CA_FILE     ca to verify
  --protocol PROTOCOL   https or http
  --trace               print trace of soap request and response

Commands:
  cluster-dev    get cluster devices
  cluster-info   get cluster info
  complete       print bash completion command
  dev-exists     check if a given device exists in landb
  dev-info       get info on a device
  dev-managed    set/unset a device as managed in landb
  dev-random     generate a random vm name which does not exist in landb
  dev-setip6     set ipv6 availability on a device
  help           print detailed help for another command
  ipsrv-register register dummy vms for the given ip service
  service-info   get info passing hypervisor name or vmName of the device
  soap           trigger direct SOAPs calls and print traces
  vm-create      create a new vm in landb
  vm-delete      delete (mark as deleted) a vm in landb
```

Help on a single command:
```
# landb help dev-exists
usage: landb dev-exists [-h] device

check if a given device exists in landb

positional arguments:
  device      name of the device

optional arguments:
  -h, --help  show this help message and exit
```

## Installation

RPM available in CERN koji and repos.

Alternatively you can build and install the python package:
```
python setup.py install
```

## Requirements

python-suds, python-cliff, python-configparser

## Test with unittest

```
docker run --name landb --rm --net host gitlab-registry.cern.ch/cloud/landb
```

And run the tests
```
python setup.py test
```

## Building the rpm

An easy sequence of steps that works:
```
git clone https://gitlab.cern.ch/cloud-infrastructure/python-landbclient.git
mv python-landbclient python-landbclient-<version>
tar zcvf python-landbclient-<version>.tar.gz python-landbclient-<version>
mv python-landbclient-<version>.tar.gz ~/rpmbuild/SOURCES
cd python-landbclient-<version>
rpmbuild -bs -D 'dist .el7' python-landbclient.spec
koji build <koji-tag> python-landbclient-<version>-1.el7.cern.src.rpm 
```
 
## Contact

cloud-developers@cern.ch
