from oslo_config import cfg

from landbclient.conf import default

conf_modules = [
    default,
]

CONF = cfg.CONF

for module in conf_modules:
    module.register_opts(CONF)

CONF(args=[], project='landbclient')
