from oslo_config import cfg

ca_file = cfg.StrOpt(
    'ca_file',
    default=None,
    help="Certificate file to verify connections")

landb_host = cfg.StrOpt(
    'landb_host',
    default='network.cern.ch',
    help="Host to use for landb operations")

landb_port = cfg.IntOpt(
    'landb_port',
    default=443,
    help="Host to use for landb operations")

protocol = cfg.StrOpt(
    'protocol',
    default='https',
    help="Defalt protocol to use (http/https)")

landb_user = cfg.StrOpt(
    'landb_user',
    default='neutron',
    help="Defalt user for landb")

landb_password = cfg.StrOpt(
    'landb_password',
    default='fake_password',
    help="Defalt password for landb")

landb_version = cfg.IntOpt(
    'landb_version',
    default=6,
    help="Version of SOAP service to use for landb")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    ca_file,
    landb_host,
    landb_port,
    protocol,
    landb_user,
    landb_password,
    landb_version,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
