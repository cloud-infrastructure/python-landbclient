# Copyright 2018 CERN. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

"""Client for the CERN landb.

This module offers an API to interact with the CERN landb.
"""

import logging
import random
import string
import suds.client
import suds.wsdl
import suds.xsd.doctor

from landbclient import exception
from landbclient.transport import RequestsTransport


# pylint: disable=R0201
class LanDB(object):
    """LanDB is a python client on top of the CERN's landb SOAP interface."""

    DEFAULT_NAMEMAXTRIES = 5
    DEFAULT_NAMESIZE = 10
    DEFAULT_MAXIPSERVICE = 255
    DEFAULT_NAMESTART = 'Z'
    LANDB_MAX_REQ_SIZE = 200

    log = logging.getLogger(__name__)

    def __init__(self, username, password, host, port,
                 cert_file=None, key_file=None, ca_file=None, protocol='https',
                 version=5):
        """Initialize a landb client with the given params."""
        url = f"{protocol}://{host}:{port}"
        imp = suds.xsd.doctor.Import(
            'http://schemas.xmlsoap.org/soap/encoding/')
        try:
            self.client = suds.client.Client(
                f'{url}/sc/soap/soap.fcgi?v={version}&WSDL',
                doctor=suds.xsd.doctor.ImportDoctor(imp),
                transport=RequestsTransport(cert_file, key_file, ca_file))
            self.__auth(username, password)
        except Exception:
            # fallback for mock landb service compatibility
            try:
                self.client = suds.client.Client(
                    f'{url}/axis/services/NetworkServicePort?wsdl',
                    doctor=suds.xsd.doctor.ImportDoctor(imp),
                    transport=RequestsTransport(
                        cert_file, key_file, ca_file))
                self.__auth(username, password)
            except Exception as exc:
                raise Exception(
                    f'failed fetch wsdl from {url} (check passwd?) :: {exc}')

        self.soap = self.client.service

    def __auth(self, username, password):
        """Get token from landb."""
        token = self.client.service.getAuthToken(username, password, 'CERN')
        self.client.set_options(soapheaders=dict(Auth={'token': token}))
        return token

    def device_random(self, maxtries=DEFAULT_NAMEMAXTRIES,
                      namesize=DEFAULT_NAMESIZE, namestart=DEFAULT_NAMESTART):
        """Generate and returns a random non existing device name in landb."""
        for _ in range(maxtries):
            dev_temp = ''.join(
                random.choice(  # nosec
                    string.ascii_uppercase + string.digits
                ) for x in range(namesize))
            dev_name = namestart + dev_temp
            if not self.device_exists(dev_name):
                return dev_name
        raise Exception('failed to generate a non existing name '
                        f'after {maxtries} tries')

    def device_exists(self, name):
        """Return true if the given device exists in landb."""
        try:
            res = self.client.service.getDeviceInfo(name)
            if res is None:
                return False
        except Exception:
            return False
        return True

    def device_info(self, name):
        """Return info on a device."""
        result = self.client.service.getDeviceInfo(name)
        if not result:
            raise Exception(f'failed to get device info :: {name}')
        return result

    def device_managed(self, name, state='true'):
        """Set or unset if a device is managed in landb."""
        result = self.client.service.vmSetUnsetManagedFlag(name, state)
        if not result:
            raise Exception(f'failed to set device managed :: {name}')

    def device_hostname(self, address):
        """Get the hostname(s) given an IP."""
        result = self.device_search(ip_addr=address)
        return result if result else None

    def device_search(self, ip_addr='', mac='', surname='',
                      serialnumber='', tag='', name=''):
        """Get the hostname given an IP, mac or surname."""
        device = self.client.service.searchDevice({
            'Name': name,
            'IPAddress': ip_addr,
            'HardwareAddress': mac,
            'Surname': surname,
            'SerialNumber': serialnumber,
            'Tag': tag,
        })
        if device:
            return device
        return None

    def device_ipv6ready(self, device, state):
        """Update ipv6 ready flag on the device."""
        return self.client.service.deviceUpdateIPv6Ready(device, state)

    def device_create(self, device_name, location, manufacturer, model, os,
                      responsible, description='', tag='', mainuser=None,
                      manager=None, ipv6ready=True, manager_locked=True,
                      serialnumber=''):
        """Create a device in landb.

        location shall be like {'Floor':'0', 'Room':'0', 'Building':'0'}
        os shall be {'Name':'LINUX', 'Version':'UNKNOWN'}
        responsible and mainuser shall be like:
          {'Name': 'Smith', 'FirstName': 'Alice', 'Department': 'IT',
           'Group': 'OIS', 'PersonID': 'id'}
            For a person {'PersonID': 123456} is enough
            For e-group {'FirstName': 'E-GROUP', 'Name': 'your-egroup-name'}
        """
        if manager_locked and not manager:
            manager = {'FirstName': 'E-GROUP',
                       'Name': 'AI-OPENSTACK-ADMIN',
                       'Department': 'IT'}

        device = {'DeviceName': device_name,
                  'Location': location,
                  'Manufacturer': manufacturer,
                  'Model': model,
                  'Description': description,
                  'Tag': tag,
                  'OperatingSystem': os,
                  'ResponsiblePerson': responsible,
                  'UserPerson': mainuser,
                  'LandbManagerPerson': manager,
                  'IPv6Ready': ipv6ready,
                  'ManagerLocked': manager_locked,
                  'SerialNumber': serialnumber}

        # result = self.client.service.deviceInsert(device)
        result = self.client.service.vmCreate(device,
                                              {'VMParent': None})

        if not result:
            raise Exception(
                f"failed to create device :: {device_name} :: {result.fault}")

    def device_delete(self, device_name, verify_no_interfaces=True):
        """Remove a device (and all its card/interfaces) from LanDB.

        If verify_no_interfaces is set, it will check that no interfaces are
        remaining on the device
        """
        if verify_no_interfaces:
            # device_info will also throw exception if not found
            device = self.device_info(device_name)
            if device.Interfaces and len(device.Interfaces) > 0:
                raise Exception("Verification failed at device_delete. There "
                                "are interfaces left on the device.")

        result = self.client.service.vmDestroy(device_name)
        # result = self.client.service.deviceRemove(device_name)

        if not result:
            raise Exception(
                f"failed to delete device :: {device_name} :: {result.fault}")

    def device_addinterface(self, device_name, interface_name, ipv4, ipv6,
                            service_name, internet_connectivity, parent=None,
                            security_class="USER", vm_cluster=None):
        """Add an interface to LanDB."""
        if not interface_name.upper().endswith(".CERN.CH"):
            interface_name += ".CERN.CH"

        # This is the code to use, when we can use logicalInterface
        # logical_interface = {
        #     'InterfaceName': interface_name,
        #     'ServiceName': service_name,
        #     'SecurityClass': security_class,
        #     'IP': ipv4,
        #     'IPv6': ipv6,
        #     'InternetConnectivity': internet_connectivity
        # }

        # result = self.client.service.deviceAddLogicalInterface(
        #     device_name, logical_interface)

        # vm_cluster = None
        if parent:
            vm_cluster = self.cluster_by_service_host(parent, service_name)

        if not vm_cluster:
            raise Exception("Cluster for Hypervisor not found in LanDB")

        # For the vmAddInterface we have to pass a string,
        # for deviceAddLogicalInterface a boolean
        if internet_connectivity is True:
            internet_connectivity = 'Y'
        else:
            internet_connectivity = 'N'

        vm_interface_options = {
            'IP': ipv4,
            'IPv6': ipv6,
            'ServiceName': service_name,
            'InternetConnectivity': internet_connectivity
        }

        result = self.client.service.vmAddInterface(
            device_name, interface_name, vm_cluster, vm_interface_options)
        if not result:
            raise Exception("failed to add interface :: %s" % device_name)
        return result

    def device_moveinterface(self, device, interface_name):
        """Move Interface."""
        if not interface_name.upper().endswith(".CERN.CH"):
            interface_name += ".CERN.CH"
        try:
            self.client.service.interfaceMove(interface_name, device)
        except Exception as e:
            self.log.error("Cannot move interface - %s", e)
            raise exception.CernLanDB()

    def device_removeinterface(self, device_name, interface_name, ip_address):
        """Remove an interface from LanDB."""
        if not interface_name.upper().endswith(".CERN.CH"):
            interface_name += ".CERN.CH"
        if ip_address:
            device_info = self.client.service.getDeviceInfo(device_name)
            if device_info.Interfaces:
                for i in device_info.Interfaces:
                    if ((i.IPAddress == ip_address)
                            or (i.IPv6Address == ip_address)):
                        if not interface_name:
                            interface_name = i.Name
                        elif interface_name.upper() != i.Name.upper():
                            self.log.error(
                                "Interface name mismatch, using LanDB name: "
                                f"{interface_name} vs {i.Name}")
                            interface_name = i.Name
                        break
            else:
                self.log.error("Cannot find device interface for IP: %s",
                               ip_address)
                raise exception.CernLanDBUpdate()

        result = self.client.service.vmRemoveInterface(
            device_name, interface_name)
        # This is the code to use, when we can use logicalInterface
        # result = self.client.service.deviceRemoveLogicalInterface(
        #     device_name, interface_name)
        if not result:
            raise Exception("failed to remove interface :: %s" % device_name)
        return result

    def device_addcard(self, device_name, mac):
        """Add a card (MAC address) to a device in LanDB."""
        result = self.client.service.deviceAddCard(
            device_name, {"HardwareAddress": mac, "CardType": "Ethernet"})
        if not result:
            raise Exception("failed to add device card :: %s" % device_name)
        return result

    def device_removecard(self, device_name, mac):
        """Remove a card (MAC address) from a device in LanDB."""
        result = self.client.service.deviceRemoveCard(device_name, mac)
        if not result:
            raise Exception("failed to remove device card :: %s" % device_name)
        return result

    def bind_unbind_interface(self, interface_name, mac):
        """Binds an interface to a card. Empty MAC unsets the binding."""
        if not interface_name.upper().endswith(".CERN.CH"):
            interface_name += ".CERN.CH"
        try:
            result = self.client.service.bindUnbindInterface(interface_name,
                                                             mac)
            if not result:
                raise Exception('failed to bind interface :: '
                                f'{interface_name} with {mac}')

            return result
        except Exception as e:
            self.log.error("Cannot bind interface - %s", e)
            raise exception.CernLanDB()

    def vm_create(self, vm_name, vm_location, vm_manufacturer, vm_model,
                  vm_os, vm_responsible, vm_mac, vm_cluster, vm_parent,
                  vm_description='', vm_tag='', vm_user=None, vm_ip='',
                  vm_ipv6='', vm_ipservice='', vm_internet=True,
                  vm_landb_manager=None, ipv6ready=True, manager_locked=False,
                  vm_domain_name='cern.ch', vm_alias=[], vm_serialnumber=''):
        """Create a vm in landb.

        vm_location shall be like {'Floor':'0', 'Room':'0', 'Building':'0'}
        vm_os shall be {'Name':'LINUX', 'Version':'UNKNOWN'}
        vm_responsible and vm_user shall be like:
          {'Name': 'Smith', 'FirstName': 'Alice', 'Department': 'IT',
           'Group': 'OIS', 'PersonID': 'id'}
            Only 'Name' and 'FirstName' are mandatory
        """
        vm_device = {'DeviceName': vm_name,
                     'Location': vm_location,
                     'Manufacturer': vm_manufacturer,
                     'Model': vm_model,
                     'Description': vm_description,
                     'Tag': vm_tag,
                     'OperatingSystem': vm_os,
                     'ResponsiblePerson': vm_responsible,
                     'UserPerson': vm_user,
                     'LandbManagerPerson': vm_landb_manager,
                     'IPv6Ready': ipv6ready,
                     'ManagerLocked': manager_locked,
                     'SerialNumber': vm_serialnumber}

        vm_create_options = {'VMParent': vm_parent}
        vm_interface_name = f'{vm_name}.{vm_domain_name}'

        # add the vm without an interface, in the future will be unlinked
        result = self.client.service.vmCreate(vm_device, vm_create_options)

        if not result:
            raise Exception(
                f'failed to create vm :: {vm_name} :: {result.fault}')

        # add the first interface, attached to the vm above
        try:
            result = self.vm_add_interface(
                vm_name, vm_cluster, vm_mac, vm_ip,
                vm_ipv6, vm_ipservice, vm_interface_name,
                vm_internet=vm_internet)
        except Exception as exc:
            self.vm_delete(vm_name)
            raise exc

        # add alias to the interface
        try:
            for alias in vm_alias:
                self.__set_alias(vm_name, alias)
        except Exception as exc:
            raise exc
        return result

    def vm_add_interface(
            self, vm_device, vm_cluster, vm_mac='', vm_ip='',
            vm_ipv6='', vm_ipservice='', vm_interface_name='Ethernet',
            vm_internet=True, interface_only=False):
        """Add interface vm in landb."""
        if vm_internet is True:
            vm_internet = 'Y'
        else:
            vm_internet = 'N'

        vm_interface_options = {
            'IP': vm_ip,
            'IPv6': vm_ipv6,
            'ServiceName': vm_ipservice,
            'InternetConnectivity': vm_internet,
            'BindHardwareAddress': vm_mac.replace(':', '-')
        }

        if not interface_only:
            result = self.vm_add_card(vm_device, vm_mac)

        result = self.client.service.vmAddInterface(
            vm_device, vm_interface_name, vm_cluster, vm_interface_options)
        if not result:
            raise Exception(f'failed to add vm interface :: {vm_device}')
        return result

    def vm_remove_interface(self, vm_device, vm_interface_name='', vm_mac=''):
        """Remove interface vm in landb."""
        result = self.client.service.vmRemoveInterface(
            vm_device, vm_interface_name)
        if not result:
            raise Exception('failed to delete interface :: '
                            f'{vm_interface_name} on {vm_device}')

        result = self.client.service.vmRemoveCard(vm_device, vm_mac)
        if not result:
            raise Exception(f'failed to remove interface card :: {vm_device}')

        return result

    def vm_add_card(self, vm_device, mac_address):
        """Add card on device in landb."""
        result = self.client.service.vmAddCard(
            vm_device, {
                "HardwareAddress": mac_address,
                "CardType": "Ethernet"
            })
        if not result:
            raise Exception(f'failed to add interface card :: {vm_device}')
        return result

    def vm_remove_card(self, vm_device, mac_address):
        """Remove card from device in landb."""
        result = self.client.service.vmRemoveCard(
            vm_device, mac_address)

        if not result:
            raise Exception(f'failed to remove card :: {vm_device} '
                            f'with {mac_address}')
        return result

    def vm_update(self, device, new_device=None,
                  location={'Floor': '0', 'Room': '0', 'Building': '0'},
                  manufacter='KVM', model='VIRTUAL MACHINE', description=None,
                  tag='OPENSTACK VM', operating_system=None,
                  responsible_person=None, user_person=None, ipv6ready=None,
                  manager=None, manager_locked=None):
        """Update vm metadata in landb."""
        metadata = None
        try:
            metadata = self.client.service.getDeviceBasicInfo(device.upper())
        except Exception:  # nosec
            pass

        if new_device is None:
            new_device = device

        if description is None and metadata is not None:
            description = metadata.Description \
                if metadata.Description is not None else ''

        if operating_system is None and metadata is not None:
            operating_system = metadata.OperatingSystem

        if responsible_person is None and metadata is not None:
            responsible_person = metadata.ResponsiblePerson

        if user_person is None and metadata is not None:
            user_person = metadata.UserPerson

        if ipv6ready is None and metadata is not None:
            ipv6ready = metadata.IPv6Ready

        if manager is None and metadata is not None:
            manager = metadata.LandbManagerPerson

        if manager_locked is None and metadata is not None:
            manager_locked = metadata.ManagerLocked

        result = self.client.service.vmUpdate(
            device,
            {
                'DeviceName': new_device,
                'Location': location,
                'Manufacturer': manufacter,
                'Model': model,
                'Description': description,
                'Tag': tag,
                'OperatingSystem': operating_system,
                'ResponsiblePerson': responsible_person,
                'UserPerson': user_person,
                'IPv6Ready': ipv6ready,
                'LandbManagerPerson': manager,
                'ManagerLocked': manager_locked
            }
        )
        return result

    def vm_delete(self, device, destroy=True, new_device=None):
        """Delete or mark as delete vms according to destroy flag."""
        if destroy is True:
            # FIXME: figure out netreset equivalent in soapv6
            # reset = self.client.service.vmNetReset(device)
            result = self.client.service.vmDestroy(device)
        else:
            if new_device is None:
                new_device = self.device_random()

            osystem = {'Name': 'UNKNOWN', 'Version': 'UNKNOWN'}
            responsible = {'FirstName': 'E-GROUP',
                           'Name': 'AI-OPENSTACK-ADMIN',
                           'Department': 'IT'}
            user_person = {'FirstName': 'E-GROUP',
                           'Name': 'AI-OPENSTACK-ADMIN',
                           'Department': 'IT'}
            # FIXME: figure out netreset equivalent in soapv6
            # reset = self.client.service.vmNetReset(device)
            result = self.vm_update(device, new_device=new_device,
                                    description='Not in use',
                                    operating_system=osystem,
                                    responsible_person=responsible,
                                    user_person=user_person)
        # FIXME: figure out netreset equivalent in soapv6
        # if not reset:
        #    raise Exception(f"failed to reset network on vm :: {device}")
        if not result:
            raise Exception(f'failed to delete vm :: {device}')
        return result

    def vm_migrate(self, hostname, node):
        """Migrate vm with hostname to node."""
        self.client.service.vmMigrate(hostname, node)

    def cluster_devices(self, cluster, detail=False):
        """Get all cluster devices."""
        devices = self.client.service.vmClusterGetDevices(cluster)
        if not detail:
            return devices

        device_details, i = [], 0
        while i < len(devices):
            nxt = min(len(devices), i + self.LANDB_MAX_REQ_SIZE)
            device_details.extend(
                self.client.service.getDeviceInfoArray(devices[i:nxt]))
            i = nxt

        result = {}
        for dev in device_details:
            result[dev.DeviceName.lower()] = dev
        return result

    def cluster_info(self, cluster=None, device=None):
        """Get cluster info."""
        if device is not None:
            cluster = self.client.service.vmGetClusterMembership(device)[0]

        if cluster is None:
            raise Exception("No device or cluster provided, need at least one")

        return self.client.service.vmClusterGetInfo(cluster)

    def cluster_by_service_host(self, hostname: str, service_name: str):
        """Find cluster by service & hostname.

        The function returns the LanDB cluster name associated with the
        hostname. If more than one cluster is associated with the host,
        service_name is used to filter the correct one.
        """
        host = hostname.split('.')[0]
        clusters = self.client.service.vmGetClusterMembership(host)

        if not clusters or len(clusters) == 0:
            return None

        if len(clusters) == 1:
            # We assume the only cluster contains the required service
            return clusters[0]

        # For hosts that have multiple VMPools, we need to find the right one
        for c_name in clusters:
            c_info = self.client.service.vmClusterGetInfo(c_name)
            if service_name in c_info.Services:
                return c_name

        return None

    def ipservice_register(
            self, ipservice, parent,
            location={'Floor': '0', 'Room': '0', 'Building': '0'},
            manufacturer='KVM', model='VIRTUAL MACHINE',
            osystem={'Name': 'UNKNOWN', 'Version': 'UNKNOWN'},
            responsible={'FirstName': 'E-GROUP', 'Name': 'AI-OPENSTACK-ADMIN',
                         'Department': 'IT'},
            mac='02-16-3e',
            maxipsrv=DEFAULT_MAXIPSERVICE):
        """Register a new ip service in landb (fill in with dummy vms)."""
        for i in range(1, maxipsrv):
            vmname = self.device_random()
            self.vm_create(
                vmname, location, manufacturer, model, osystem,
                responsible, mac, ipservice, parent, vm_description=vmname,
                vm_tag=vmname, vm_user='mock', vm_ip=f"192.168.1.{i}",
                vm_ipv6=f"2002::{i}", vm_ipservice=ipservice,
                vm_internet=True)
            self.device_managed(vmname)

    def ipservice_info(self, service):
        """Get service information."""
        return self.client.service.getServiceInfo(service)

    def ipservice_devices(self, service):
        """Get devices from service."""
        return self.client.service.getDevicesFromService(service)

    def _verify_and_return_interface(self, interface_name):
        devices = self.client.service.searchDevice({'Name': interface_name})
        if len(devices) != 1:
            raise Exception(f"Interface {interface_name} not found")

        device_info = self.device_info(devices[0])
        for i in device_info.Interfaces:
            if i.Name.upper() == interface_name.upper():
                self.log.debug("Interface search: found")
                return i

        self.log.error(f"Interface {interface_name} not found")
        raise Exception(f"Interface {interface_name} not found")
        return None

    def alias_update(self, interface_name, new_alias):
        """Update alias."""
        try:
            interface_info = self._verify_and_return_interface(interface_name)
            old_alias = interface_info.IPAliases

            if old_alias is None:
                old_alias = []

            old_alias_set = set([x.upper() for x in old_alias])
            new_alias_set = set(
                [x.upper() for x in new_alias if x != '' and '.' in x]
                + [x.upper() + '.CERN.CH'
                    for x in new_alias if x != '' and '.' not in x])

            interface_domain = interface_name.split('.', 1)[1].strip().upper()
            for alias_temp in new_alias_set:
                if not alias_temp.upper().endswith(interface_domain):
                    msg = (f"{alias_temp} - Alias domain doesn't match "
                           "interface domain")
                    self.log.error(msg)
                    raise Exception(msg)

            add_alias = new_alias_set - old_alias_set
            remove_alias = old_alias_set - new_alias_set

            self.log.debug(f"alias_update: new_alias_set: {new_alias_set}")
            self.log.debug(f"alias_update: old_alias_set: {old_alias_set}")

            for alias in add_alias:
                if self.device_search(name=alias):
                    self.log.error("Alias already exists: %s", str(alias))
                    msg = (f'{alias} - The device already exists or is not '
                           'a valid hostname')
                    raise Exception(msg)

            try:
                for alias in remove_alias:
                    self.__unset_alias(interface_name, alias)

                for alias in add_alias:
                    self.__set_alias(interface_name, alias)
            except Exception:
                self.__recover_alias(interface_name, old_alias)
                msg = (f'{str(alias)} - The device already exists or is not '
                       'a valid hostname')
                raise Exception(msg)
        except Exception as e:  # nosec
            self.log.error(str(e))
            pass
        # FIXME: throw proper raise exception.CernInvalidHostname()
#        except exception.CernInvalidHostname:
#             msg = _(f'{str(alias)} - The device already exists or is not '
#                     'a valid hostname')
#             raise exception.CernInvalidHostname(msg)

    def __recover_alias(self, interface_name, old_alias):
        """Try to recover alias after an error."""
        self.log.info("Trying to recover old alias")
        landb_interface = self._verify_and_return_interface(interface_name)
        current_alias = landb_interface.IPAliases

        if current_alias:
            for alias in current_alias:
                self.__unset_alias(interface_name, alias)
        try:
            for alias in old_alias:
                self.__set_alias(interface_name, alias)
        except Exception as e:
            self.log.error("Cannot recover all alias: %s", e)
            raise Exception("Cannot recover all aliases", e)

    def __set_alias(self, interface_name, alias):
        """Set alias to a interface."""
        try:
            if not interface_name.lower().endswith(".cern.ch"):
                interface_name += ".cern.ch"
            if not alias.lower().endswith(".cern.ch"):
                alias += ".cern.ch"
            self.log.debug(f"Adding alias {alias} to {interface_name}")
            self.client.service.interfaceAddAlias(interface_name, alias)
        except Exception as exc:
            raise Exception(f'Impossible to set alias {alias} '
                            f'on interface {interface_name} :: {exc}')

        # FIXME: throw proper raise exception.CernLanDBUpdate()
#        except Exception as e:
#            raise exception.CernLanDBUpdate()

    def __unset_alias(self, interface_name, alias):
        """Unset all alias in a interface."""
        try:
            if not interface_name.lower().endswith(".cern.ch"):
                interface_name += ".cern.ch"
            if not alias.lower().endswith(".cern.ch"):
                alias += ".cern.ch"
            self.log.debug(f'Removing alias {alias} to {interface_name}')
            self.client.service.interfaceRemoveAlias(interface_name, alias)
        except Exception as exc:
            raise Exception(f'Impossible to remove alias {alias} '
                            f'on intf {interface_name} :: {exc}')
        # FIXME: throw proper raise exception.CernLanDBUpdate()
#            self.log.error(_("Cannot unset alias in landb: %s" % str(e)))
#            raise exception.CernLanDBUpdate()

    def vm_get_cluster_membership(self, hostname):
        """Get the list of clusters where this physical host is on."""
        host = hostname.split(".")[0]
        return self.client.service.vmGetClusterMembership(host)

    def service_info(self, cluster=None, device=None):
        """Get the service info.

        If device is passed, it can be a VM or a hypervisor and the info of all
        services in its cluster are returned.
        If cluster is passed, the info on all its services are returned.

        """
        cluster_info = self.cluster_info(cluster=cluster, device=device)

        if cluster_info is None:
            raise Exception(f'Cluster {cluster} was not found')
        if not cluster_info['Services']:
            raise Exception(f'Cluster {cluster} has no services')

        services = {}
        for service in cluster_info['Services']:
            services[service] = self.client.service.getServiceInfo(service)
        return {'cluster': cluster_info['Name'], 'services': services}

    def get_setinfo(self, set_name):
        """Get landb set info."""
        result = self.client.service.getSetInfo(set_name.lower())
        if not result:
            raise Exception(f'Failed to get set info :: {set_name}')
        return result

    def _get_projects_set(self, set_name):
        try:
            description = self.client.service.getSetInfo(
                set_name.lower()).Description
        except Exception:
            self.log.error("Can't get the lanDB set description: %s",
                           set_name)
            return []

        os_projects = []
        if 'openstack_project' in description.lower().replace(" ", ""):
            try:
                os_temp1 = description.lower().split(
                    'openstack_project', 1)[1].strip()
                os_temp2 = os_temp1.split('=', 1)[1].strip()
                os_temp3 = os_temp2.split(';', 1)[0].strip()
                os_temp4 = os_temp3.split(',')
                os_projects = [i.strip() for i in os_temp4]
            except Exception:
                self.log.error("Can't parse lanDB set description: %s",
                               set_name)
                return []

        return os_projects

    def get_addresses_set(self, set_name):
        addresses = []
        try:
            addresses = self.client.service.getSetInfo(
                set_name.lower()).Addresses
        except Exception:
            self.log.warning("Can't get the addresses from the lanDB set: %s",
                             set_name)
        return addresses

    def insert_address_set(self, set_name, interface_name, device=None):
        """Insert an address in a set."""
        if device:
            self._verify_and_return_interface(device, interface_name)
        try:
            self.client.service.setInsertAddress(set_name, interface_name)
        except Exception as exc:
            self.log.error("Can't insert interface %s in lanDB set: %s",
                           interface_name, set_name)
            raise Exception(f'Failed to add interface {interface_name} '
                            f'on set {set_name} :: {exc}')

    def delete_address_set(self, set_name, interface_name, device=None):
        """Delete an address from a set."""
        if device:
            self._verify_and_return_interface(device, interface_name)
        try:
            self.client.service.setDeleteAddress(set_name, interface_name)
        except Exception:
            self.log.error("Can't delete interface %s in lanDB set: %s",
                           interface_name, set_name)

    def update_address_set(self, set_name, project_id, interface_name):
        """Update set with new address."""
        set_projects = self._get_projects_set(set_name)
        if project_id.lower() in set_projects:
            self.insert_address_set(set_name, interface_name)
        else:
            raise Exception(f'Failed to add interface {interface_name} '
                            f'on set {set_name}')

    def device_update(self, device, new_device=None, location=None,
                      manufacter=None, model=None, description=None,
                      operating_system=None, responsible_person=None,
                      user_person=None, ipv6ready=None):
        """Update physical node metadata in landb."""
        metadata = self.client.service.getDeviceInfo(device.upper())
        interface_rename = False
        if new_device is None:
            new_device = device
        else:
            interface_rename = True

        if description is None:
            description = (metadata.Description
                           if metadata.Description is not None else '')

        if operating_system is None:
            operating_system = metadata.OperatingSystem

        if responsible_person is None:
            responsible_person = metadata.ResponsiblePerson

        if user_person is None:
            user_person = metadata.UserPerson

        if ipv6ready is None:
            ipv6ready = metadata.IPv6Ready

        manager_person = {'FirstName': 'E-GROUP',
                          'Name': 'AI-OPENSTACK-ADMIN',
                          'Department': 'IT'}

        try:
            self.client.service.deviceUpdate(device, {
                'DeviceName': new_device,
                'Location': metadata.Location,
                'Zone': metadata.Zone,
                'Manufacturer': metadata.Manufacturer,
                'Model': metadata.Model,
                'SerialNumber': metadata.SerialNumber,
                'Description': description,
                'Tag': 'OPENSTACK IRONIC NODE',
                'OperatingSystem': operating_system,
                'ResponsiblePerson': responsible_person,
                'UserPerson': user_person,
                'LandbManagerPerson': manager_person,
                'IPv6Ready': ipv6ready,
                'HCPResponse': 'true'
            })
        except Exception as e:
            self.log.error("Cannot update landb: %s", e)
            raise exception.CernLanDBUpdate(e)

        if interface_rename:
            for i in metadata.Interfaces:
                if i.IPAliases is not None:
                    for alias in i.IPAliases:
                        self.__unset_alias(i.Name, alias)
                if device.upper() + '-IPMI.CERN.CH' == i.Name.upper():
                    self.interface_rename(i.Name, new_device + '-IPMI.CERN.CH')
                    self.__set_alias(
                        new_device + '-IPMI.CERN.CH',
                        metadata.SerialNumber + '-IPMI.CERN.CH')
                elif device.upper() + '.CERN.CH' == i.Name.upper():
                    try:
                        self.interface_rename(i.Name, new_device + '.CERN.CH')
                    except Exception:
                        self.log.error("Can't rename interface: %s", i.Name)
                    try:
                        self.__set_alias(
                            new_device + '.CERN.CH',
                            metadata.SerialNumber + '.CERN.CH')
                    except Exception:
                        self.log.error("Can't set alias for interface: %s",
                                       new_device + '.CERN.CH')
                else:
                    self.log.warning("Device %s has multiple interfaces "
                                     "registered in LanDB: %s", device, i.Name)

    def device_rename_ironic(self, device, new_device=None):
        """Rename a device in LanDB and keep all names."""
        if new_device is None:
            for i in range(5):
                # nosec B311 (allow random here for non security purposes)
                new_device = 'P' + ''.join(random.choice(  # nosec B311
                    string.ascii_uppercase + string.digits) for x in range(10))
                self.log.debug("Random instance name: %s", new_device)
                if self.device_exists(new_device):
                    self.log.debug("Hostname already exists: %s", new_device)
                    continue
                else:
                    break

        os = {'Name': 'LINUX',
              'Version': 'UNKNOWN'}

        responsible = {'FirstName': 'E-GROUP',
                       'Name': 'AI-OPENSTACK-ADMIN',
                       'Department': 'IT'}

        user_person = {'FirstName': 'E-GROUP',
                       'Name': 'AI-OPENSTACK-ADMIN',
                       'Department': 'IT'}

        try:
            self.device_update(device,
                               new_device=new_device,
                               description='Not in use',
                               operating_system=os,
                               responsible_person=responsible,
                               user_person=user_person,
                               ipv6ready=True)
            # TODO(dfailing) remove alias and set
        except Exception as e:
            self.log.error("Cannot update device in landb: %s", e)
#            raise exception.CernLanDBUpdate()
            pass

    def interface_rename(self, interface, new_interface):
        """Rename an interface."""
        try:
            self.client.service.interfaceRename(interface, new_interface)
        except Exception as e:
            self.log.warning("Cannot rename interface in lanDB: %s", e)
            return False
        return True

    def isIronic(self, device):
        """Check whether the device is managed by Ironic."""
        try:
            metadata = self.client.service.getDeviceBasicInfo(device.upper())
            if metadata.Tag == 'OPENSTACK IRONIC NODE':
                return True
        except Exception as e:
            self.log.error("Cannot get lanDB metadata: %s - %s", device, e)
            raise exception.CernLanDB()
        return False
