# Copyright 2018 CERN. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

"""Set of commands for the landb cli."""

import logging

from cliff.command import Command
from cliff.show import ShowOne


class Soap(Command):
    """Trigger direct SOAPs calls and print traces."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(Soap, self).get_parser(prog_name)
        parser.add_argument('soapmethod', type=str, help='soap method to call')
        parser.add_argument(
            'soapargs', type=str, nargs='+',
            help='arguments to the soap call')
        return parser

    def take_action(self, parsed_args):
        soap = self.app.landb.soap
        method = getattr(soap, parsed_args.soapmethod)
        result = method(*parsed_args.soapargs)
        self.app.stdout.write(f"{result}\n")


class ClusterDevices(Command):
    """Get cluster devices."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(ClusterDevices, self).get_parser(prog_name)
        parser.add_argument('cluster', type=str, help='name of the cluster')
        return parser

    def take_action(self, parsed_args):
        cluster = self.app.landb.cluster_devices(parsed_args.cluster)
        self.app.stdout.write(f"{cluster}\n")


class ClusterInfo(Command):
    """Get cluster info."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(ClusterInfo, self).get_parser(prog_name)
        parser.add_argument('cluster', type=str, help='name of the cluster')
        parser.add_argument(
            'device', default=None, type=str,
            help='hypervisor name or vmName of the device')
        return parser

    def take_action(self, parsed_args):
        cluster = self.app.landb.cluster_info(
            cluster=parsed_args.cluster, device=parsed_args.device)
        self.app.stdout.write(f"{cluster}\n")


class DeviceManaged(Command):
    """Set/unset a device as managed in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeviceManaged, self).get_parser(prog_name)
        parser.add_argument('device', type=str, help='name of the device')
        parser.add_argument('--false', type=bool, help='set managed to false')
        return parser

    def take_action(self, parsed_args):
        state = 'true'
        if parsed_args.false:
            state = 'false'
        self.app.landb.device_managed(parsed_args.device, state)


class DeviceRandom(ShowOne):
    """Generate a random vm name which does not exist in landb."""

    log = logging.getLogger(__name__)

    def take_action(self, parsed_args):
        return (('Device Name'), (self.app.landb.device_random()))


class DeviceExists(Command):
    """Check if a given device exists in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeviceExists, self).get_parser(prog_name)
        parser.add_argument('device', type=str, help='name of the device')
        return parser

    def take_action(self, parsed_args):
        result = self.app.landb.device_exists(parsed_args.device)
        if not result:
            raise Exception(
                f"device {parsed_args.device} does not exist in landb")


class DeviceSearch(Command):
    """Search for a given device based on properties."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeviceSearch, self).get_parser(prog_name)
        parser.add_argument(
            '--name', default='', type=str,
            help='name of the device,interface or alias')
        parser.add_argument(
            '--ip', default='', type=str, help='ip address of the device')
        parser.add_argument(
            '--mac', default='', type=str, help='mac of the device')
        parser.add_argument(
            '--surname', default='', type=str, help='surname of the owner')
        parser.add_argument(
            '--serialnumber', default='', type=str,
            help='serial number of the device')
        parser.add_argument(
            '--tag', default='', type=str, help='device tag')
        return parser

    def take_action(self, parsed_args):
        result = self.app.landb.device_search(
            name=parsed_args.name,
            ip_addr=parsed_args.ip,
            mac=parsed_args.mac,
            surname=parsed_args.surname,
            serialnumber=parsed_args.serialnumber,
            tag=parsed_args.tag)
        if not result:
            raise Exception(
                f"no device matching IP '{parsed_args.ip}' "
                f"or MAC '{parsed_args.mac}' "
                f"or SURNAME '{parsed_args.surname}' "
                f"or SERIALNUMBER '{parsed_args.serialnumber}'"
                f"or TAG '{parsed_args.tag}' in landb")
        self.app.stdout.write(f"{result}\n")


class DeviceInfo(Command):
    """Get info on a device."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeviceInfo, self).get_parser(prog_name)
        parser.add_argument('device', type=str, help='name of the device')
        return parser

    def take_action(self, parsed_args):
        result = self.app.landb.device_info(parsed_args.device)
        if not result:
            raise Exception(
                f"device {parsed_args.device}"
                "does not exist in landb")
        self.app.stdout.write(f"{result}\n")


class DeviceIPv6(Command):
    """set ipv6 availability on a device."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeviceIPv6, self).get_parser(prog_name)
        parser.add_argument('device', type=str, help='name of the device')
        parser.add_argument('state', type=bool, help='set flag if ipv6 ready')
        return parser

    def take_action(self, parsed_args):
        result = self.app.landb.device_ipv6ready(
            parsed_args.device, parsed_args.state)
        if not result:
            raise Exception(
                "failed to update ipv6 ready flag :: "
                f"{parsed_args.device}")


class VMCreate(Command):
    """Create a new vm in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(VMCreate, self).get_parser(prog_name)
        parser.add_argument('name', type=str, help='name of the device')
        parser.add_argument(
            'ipservice', type=str, help='ipservice of the device')
        parser.add_argument(
            'ipaddress', type=str, help='ip address of the device')
        parser.add_argument(
            'ipv6address', type=str, help='ipv6 address of the device')
        parser.add_argument(
            'macaddress', type=str, help='mac address of the device')
        parser.add_argument(
            'hypervisor', type=str, help='hypervisor name of the device')
        parser.add_argument(
            'cluster', type=str, help='cluster name of the device')
        parser.add_argument(
            'responsiblename', type=str,
            help='first name of the responsible of the device')
        parser.add_argument(
            'responsiblesurname', type=str,
            help='last name of the responsible of the device')
        parser.add_argument(
            '--floor', default=0, type=int, help='floor of the device')
        parser.add_argument(
            '--room', default=0, type=int, help='room of the device')
        parser.add_argument(
            '--building', default=0, type=int,
            help='building of the device')
        parser.add_argument(
            '--manufacturer', default='KVM', type=str,
            help='manufacturer of the device')
        parser.add_argument(
            '--model', default='VIRTUAL MACHINE', type=str,
            help='model of the device')
        parser.add_argument(
            '--osname', default='LINUX', type=str,
            help='os name of the device')
        parser.add_argument(
            '--osversion', default='UNKNOWN', type=str,
            help='os version of the device')
        parser.add_argument(
            '--tag', default='OPENSTACK VM', type=str,
            help='tag of the device')
        parser.add_argument(
            '--username', default=None, type=str,
            help='first name of the user of the device')
        parser.add_argument(
            '--usersurname', default=None, type=str,
            help='last name of the user of the device')
        parser.add_argument(
            '--managername', default=None, type=str,
            help='first name of the user of the device')
        parser.add_argument(
            '--managersurname', default=None, type=str,
            help='last name of the user of the device')
        parser.add_argument(
            '--managerlock', default=True, type=bool,
            help='sets the manager lock of the device')
        return parser

    def take_action(self, parsed_args):
        return self.app.landb.vm_create(
            vm_name=parsed_args.name,
            vm_ipservice=parsed_args.ipservice,
            vm_ip=parsed_args.ipaddress,
            vm_ipv6=parsed_args.ipv6address,
            vm_mac=parsed_args.macaddress,
            vm_parent=parsed_args.hypervisor,
            vm_cluster=parsed_args.cluster,
            vm_responsible={
                'FirstName': parsed_args.responsiblename,
                'Name': parsed_args.responsiblesurname
            },
            vm_location={
                'Floor': parsed_args.floor,
                'Room': parsed_args.room,
                'Building': parsed_args.building
            },
            vm_manufacturer=parsed_args.manufacturer,
            vm_model=parsed_args.model,
            vm_os={
                'Name': parsed_args.osname,
                'Version': parsed_args.osversion
            },
            vm_user={
                'FirstName': parsed_args.username,
                'Name': parsed_args.usersurname},
            vm_landb_manager={
                'FirstName': parsed_args.managername,
                'Name': parsed_args.managersurname
            },
            vm_tag=parsed_args.tag,
            manager_locked=parsed_args.managerlock
        )


class VMAddInterface(Command):
    """Adds an interface on a VM in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(VMAddInterface, self).get_parser(prog_name)
        parser.add_argument(
            'device', type=str, help='name of the device')
        parser.add_argument(
            'interface_name', type=str, help='name of the interface')
        parser.add_argument(
            'ipservice', type=str, help='ipservice of the device')
        parser.add_argument(
            'ipaddress', type=str, help='new ip address of the device')
        parser.add_argument(
            'ipaddress6', type=str, help='new ip address of the device')
        parser.add_argument(
            'macaddress', type=str, help='mac address of the device')
        parser.add_argument(
            'cluster', type=str, help='cluster name of the device')
        parser.add_argument(
            '--interface-only', default=False, type=bool,
            help=('Adds just the interface into the device'
                  'by default it also adds the card'))
        return parser

    def take_action(self, parsed_args):
        return self.app.landb.vm_add_interface(
            vm_device=parsed_args.device,
            vm_interface_name=parsed_args.interface_name,
            vm_ipservice=parsed_args.ipservice,
            vm_ip=parsed_args.ipaddress,
            vm_ipv6=parsed_args.ipaddress6,
            vm_mac=parsed_args.macaddress,
            vm_cluster=parsed_args.cluster,
            interface_only=parsed_args.interface_only
        )


class VMRemoveInterface(Command):
    """Removes an interface from a VM in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(VMRemoveInterface, self).get_parser(prog_name)
        parser.add_argument(
            'device', type=str, help='name of the device')
        parser.add_argument(
            'interface_name', type=str, help='name of the interface')
        parser.add_argument(
            'macaddress', type=str, help='mac address of the interface')
        return parser

    def take_action(self, parsed_args):
        return self.app.landb.vm_remove_interface(
            vm_device=parsed_args.device,
            vm_interface_name=parsed_args.interface_name,
            vm_mac=parsed_args.macaddress)


class BindInterface(Command):
    """Binds or Unbinds an interface with a mac address in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(BindInterface, self).get_parser(prog_name)
        parser.add_argument(
            'interface_name', type=str, help='name of the interface')
        parser.add_argument(
            'mac_address', type=str, help='mac address of the device')
        return parser

    def take_action(self, parsed_args):
        return self.app.landb.bind_unbind_interface(
            vm_interface_name=parsed_args.interface_name,
            mac_address=parsed_args.mac_address)


class VMAddCard(Command):
    """Adds a card on a device in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(VMAddCard, self).get_parser(prog_name)
        parser.add_argument(
            'device', type=str, help='name of the interface')
        parser.add_argument(
            'mac_address', type=str, help='mac address of the device')
        return parser

    def take_action(self, parsed_args):
        return self.app.landb.vm_add_card(
            vm_device=parsed_args.device,
            mac_address=parsed_args.mac_address)


class VMRemoveCard(Command):
    """Remove a card from a device in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(VMRemoveCard, self).get_parser(prog_name)
        parser.add_argument(
            'device', type=str, help='name of the interface')
        parser.add_argument(
            'mac_address', type=str, help='mac address of the device')
        return parser

    def take_action(self, parsed_args):
        return self.app.landb.vm_remove_card(
            vm_device=parsed_args.device,
            mac_address=parsed_args.mac_address)


class VMDelete(Command):
    """delete a vm in landb."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(VMDelete, self).get_parser(prog_name)
        parser.add_argument(
            'name', nargs='*',
            help='name of the device')
        parser.add_argument(
            '--destroy', default=True, type=bool,
            help='set to true to delete entry from landb, false to rename')
        return parser

    def take_action(self, parsed_args):
        for device in parsed_args.name:
            result = self.app.landb.vm_delete(device,
                                              destroy=parsed_args.destroy)
        return result


class VMMigrate(Command):
    """Migrate device to a different hypervisor."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(VMMigrate, self).get_parser(prog_name)
        parser.add_argument(
            'name', type=str,
            help='name of the device')
        parser.add_argument(
            'hypervisor', type=str,
            help='destination hypervisor for the migration')
        return parser

    def take_action(self, parsed_args):
        return self.app.landb.vm_migrate(
            parsed_args.name, parsed_args.hypervisor)


class IPServiceRegister(Command):
    """register dummy vms for the given ip service."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(IPServiceRegister, self).get_parser(prog_name)
        parser.add_argument('name', type=str, help='name of the ipservice')
        parser.add_argument('parent', type=str, help='parent of the device')
        return parser

    def take_action(self, parsed_args):
        return self.app.landb.ipservice_register(
            parsed_args.name, parsed_args.parent)


class ServiceInfo(Command):
    """Get the cluster info with secondary and primary service."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(ServiceInfo, self).get_parser(prog_name)
        parser.add_argument(
            '--cluster', default=None, type=str,
            help='cluster name of the device')
        parser.add_argument(
            '--device', default=None, type=str,
            help='hypervisor name or vmName of the device')
        return parser

    def take_action(self, parsed_args):
        result = self.app.landb.service_info(
            cluster=parsed_args.cluster, device=parsed_args.device)
        return result
