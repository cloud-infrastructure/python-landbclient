"""Requests transport implementation for the landb client.

Allows us to reconfigure things like certificates, logging, etc.
"""

import io
import logging
import requests
from suds.transport.http import HttpAuthenticated
from suds.transport import Reply


class RequestsTransport(HttpAuthenticated):
    """Transport implementation to reconfigure logging level."""

    def __init__(self, cert_file=None, key_file=None, ca_file=True):
        self.cert_file = cert_file
        self.key_file = key_file
        self.ca_file = ca_file
        if self.ca_file is None:
            self.ca_file = False

        logging.getLogger('requests').setLevel(logging.CRITICAL)
        logging.getLogger('urllib3').setLevel(logging.CRITICAL)
        HttpAuthenticated.__init__(self)

    def open(self, request):
        """Fetch the WSDL using cert."""
        self.addcredentials(request)
        resp = requests.get(  # nosec
            request.url,
            data=request.message,
            headers=request.headers,
            cert=(self.cert_file, self.key_file),
            verify=self.ca_file)
        return io.BytesIO(resp.content)

    def send(self, request):
        """Post to service using cert."""
        self.addcredentials(request)
        resp = requests.post(  # nosec
            request.url,
            data=request.message,
            headers=request.headers,
            cert=(self.cert_file, self.key_file),
            verify=self.ca_file)
        result = Reply(resp.status_code, resp.headers, resp.content)
        return result
