from unittest import mock

INTERFACE_DATA = {
    "Name": "interface.cern.ch",
    "IPAliases": ["old-alias.cern.ch"]
}

INTERFACE = mock.Mock(**INTERFACE_DATA)
type(INTERFACE).Name = mock.PropertyMock(
    return_value=INTERFACE_DATA['Name'])

DEVICE_DATA = {
    "Name": "device",
    "Tag": 'OPENSTACK VM',
    "Interfaces": [
        INTERFACE
    ]
}

DEVICE = mock.Mock(**DEVICE_DATA)
type(DEVICE).Name = mock.PropertyMock(
    return_value=DEVICE_DATA['Name'])

INTERFACE_IPMI_DATA = {
    "Name": "interface-ipmi.cern.ch",
    "IPAliases": ["dl120-asd.cern.ch"]
}
INTERFACE_IPMI = mock.Mock(**INTERFACE_IPMI_DATA)
type(INTERFACE_IPMI).Name = mock.PropertyMock(
    return_value=INTERFACE_IPMI_DATA['Name'])


DEVICE_IRONIC_DATA = {
    "Name": "device",
    "Tag": 'OPENSTACK IRONIC NODE',
    "Interfaces": [
        INTERFACE,
        INTERFACE_IPMI
    ]
}

DEVICE_IRONIC = mock.Mock(**DEVICE_IRONIC_DATA)
type(DEVICE_IRONIC).Name = mock.PropertyMock(
    return_value=DEVICE_IRONIC_DATA['Name'])

INTERFACE_NO_ALIAS_DATA = {
    "Name": "interface.cern.ch",
    "IPAliases": None,
}

INTERFACE_NO_ALIAS = mock.Mock(**INTERFACE_NO_ALIAS_DATA)
type(INTERFACE_NO_ALIAS).Name = mock.PropertyMock(
    return_value=INTERFACE_NO_ALIAS_DATA['Name'])

DEVICE_NO_ALIAS_DATA = {
    "Name": "device",
    "Interfaces": [
        INTERFACE_NO_ALIAS
    ]
}

DEVICE_NO_ALIAS = mock.Mock(**DEVICE_NO_ALIAS_DATA)
type(DEVICE_NO_ALIAS).Name = mock.PropertyMock(
    return_value=DEVICE_NO_ALIAS_DATA['Name'])

DEVICE_NO_INTF_DATA = {
    "Name": "device",
    "Interfaces": []
}

DEVICE_NO_INTF = mock.Mock(**DEVICE_NO_INTF_DATA)
type(DEVICE_NO_INTF).Name = mock.PropertyMock(
    return_value=DEVICE_NO_INTF_DATA['Name'])

SET_EMPTY_DATA = {
    "Name": "MYSET",
    "Description": ""
}
SET_EMPTY = mock.Mock(**SET_EMPTY_DATA)
type(SET_EMPTY).Name = mock.PropertyMock(
    return_value=SET_EMPTY_DATA['Name'])

SET_WRONG_DATA = {
    "Name": "MYSET",
    "Description": "openstack_project"
}
SET_WRONG = mock.Mock(**SET_WRONG_DATA)
type(SET_WRONG).Name = mock.PropertyMock(
    return_value=SET_WRONG_DATA['Name'])

SET_DATA = {
    "Name": "MYSET",
    "Description": "openstack_project=uuid"
}
SET = mock.Mock(**SET_DATA)
type(SET).Name = mock.PropertyMock(
    return_value=SET_DATA['Name'])
