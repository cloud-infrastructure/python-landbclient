# Copyright 2015 CERN. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

"""Cliff landb app implementation."""

import getpass
import landbclient
import landbclient.conf
import sys

from cliff.app import App
from cliff.commandmanager import CommandManager
from cliff.complete import CompleteCommand
from cliff.help import HelpCommand
from landbclient.client import LanDB

CONF = landbclient.conf.CONF


class LandbApp(App):
    """App for the landb cliff client."""

    def __init__(self):
        super(LandbApp, self).__init__(
            description="""
Client for interacting with the CERN landb.
                """,
            version=landbclient.__version__,
            command_manager=CommandManager('landb'),
        )
        self.landb = None

    def build_option_parser(self, description, version, argparse_kwargs=None):
        parser = super(LandbApp, self).build_option_parser(
            description, version, argparse_kwargs)
        parser.add_argument(
            '--landb-user',
            default=CONF.default.landb_user,
            action='store',
            help='landb username',
        )
        parser.add_argument(
            '--landb-password',
            default=CONF.default.landb_password,
            action='store',
            help='landb password',
        )
        parser.add_argument(
            '--landb-host',
            default=CONF.default.landb_host,
            action='store',
            help='landb host (default network.cern.ch)',
        )
        parser.add_argument(
            '--landb-port',
            default=CONF.default.landb_port,
            action='store',
            help='landb port (default 443)',
        )
        parser.add_argument(
            '--cert-file',
            action='store',
            help='certificate to use',
        )
        parser.add_argument(
            '--key-file',
            action='store',
            help='key to use',
        )
        parser.add_argument(
            '--ca-file',
            default=CONF.default.ca_file,
            action='store',
            help='ca to verify',
        )
        parser.add_argument(
            '--protocol',
            default=CONF.default.protocol,
            action='store',
            help='https or http',
        )
        parser.add_argument(
            '--landb-version',
            default=CONF.default.landb_version,
            type=int,
            action='store',
            help='landb version',
        )
        parser.add_argument(
            '--trace',
            default=False,
            action='store_true',
            help='print trace of soap request and response',
        )
        return parser

    def prepare_to_run_command(self, cmd):
        super(LandbApp, self).prepare_to_run_command(cmd)

        if not isinstance(cmd, HelpCommand) \
                and not isinstance(cmd, CompleteCommand) and not self.landb:
            if not self.options.landb_password:
                self.options.landb_password = getpass.getpass()
            self.landb = LanDB(
                username=self.options.landb_user,
                password=self.options.landb_password,
                host=self.options.landb_host,
                port=self.options.landb_port,
                cert_file=self.options.cert_file,
                key_file=self.options.key_file,
                ca_file=self.options.ca_file,
                protocol=self.options.protocol,
                version=self.options.landb_version)


def main(args=None):
    """Entrypoint for the landbclient."""
    return LandbApp().run(args)


if __name__ == "__main__":
    sys.exit(main())
