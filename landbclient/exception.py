class CernLanDB(Exception):
    pass


class CernLanDBAuthentication(Exception):
    pass


class CernLanDBUpdate(Exception):
    pass
